<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

//Route::get('/', function () {
//    return view('welcome');
//});

Route::post('authenticate', 'AuthenticateController@authenticate');
Route::get('user', 'AuthenticateController@getAuthenticatedUser');
Route::post('logout', 'AuthenticateController@logout');
Route::post('Login', 'UsersController@login');
Route::get('advertisments', 'AdvertismentsController@getAdvertisments');
Route::get('constants/{id}', 'CustomersController@constants');
Route::get('getCityByCountry/{id}', 'CustomersController@getCityByCountry');

Route::resource('Carriers', 'CarriersController');
Route::resource('Customers', 'CustomersController');
Route::get('transporter/trip/{id}', 'CarriersController@getTrip');
Route::post('transporter/trip/{id}', 'CarriersController@update');
Route::post('transporter/trips/search', 'CarriersController@search');
Route::put('closeExpiredTrips', 'CarriersController@closeExpiredTrips');
Route::post('custmers/trips/search', 'CustomersController@search');

Route::post('Users/profile', 'UsersController@updateProfile');
Route::get('Users/profile', 'UsersController@profile');
Route::post('Users/updatePassword', 'UsersController@updatePassword');
Route::post('Users/forgetPassword', 'UsersController@forgetPassword');
Route::post('Users/resetPassword', 'UsersController@resetPassword');
Route::resource('Users', 'UsersController');


Route::group(['middleware' => ['jwt.auth', 'auth']], function() {
Route::put('refresh/token', 'AuthenticateController@token');
Route::resource('Admins', 'AdminsController');
Route::put('cancelTripCustomer', 'CustomersController@cancelTripCustomer');
Route::post('customer/users/{id}/evaluate', 'CustomersController@evaluate');
Route::post('transporter/users/{id}/evaluate', 'CarriersController@evaluate');
Route::get('avilableTracks/{id}', 'CustomersController@avilableTracks');
Route::put('Candidates/{id}/acceptOrRefuse', 'CarriersController@acceptOrRefuseCandidates');
Route::put('Candidates/cancelRequest/{id}', 'CarriersController@candidateCancelRequest');
Route::get('Candidates/{id}', 'CarriersController@viewCandidates');
Route::get('trips/{id}/Candidates', 'CarriersController@candidates');
Route::put('endTrip', 'CarriersController@endTrip');
Route::put('confirmOrCancelCarrierTrip', 'CarriersController@confirmOrCancel');
Route::get('customerTable', 'CustomersController@table');
Route::get('trips/transporter', 'CarriersController@table');
Route::post('trips/transporter/notifyMe', 'CarriersController@notifyMe');
Route::post('trips/requests/notifyMe', 'CustomersController@notifyMe');


Route::put('confirmOrCancelRequest', 'CustomersController@confirmOrCancel');
Route::post('customers/updateTrip/{id}', 'CustomersController@update');
Route::get('customers/trips/{id}', 'CustomersController@getTrip');
Route::post('customers/trips/participate', 'CustomersController@shareTrip');

Route::resource('Advertisments', 'AdvertismentsController');
Route::resource('Notifications', 'NotificationsController');


Route::post('carriers/trips/participate', 'CarriersController@shareTrip');
Route::post('trips/transporter/add', 'CarriersController@store');

Route::post('trips/add_request', 'CustomersController@store');
Route::get('trips/requests/Candidates/{id}', 'CustomersController@viewCandidates');
Route::get('trips/requests/{id}/Candidates', 'CustomersController@candidates');
Route::put('trips/requests/candidates/{id}/acceptOrRefuse', 'CustomersController@acceptOrRefuseCandidates');
Route::put('requests/candidates/cancelRequest/{id}', 'CustomersController@candidateCancelRequest');
});