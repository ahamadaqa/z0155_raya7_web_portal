<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Users;
use App\Models\ConstantsTable;
use App\Models\Evaluations;
use App\Models\EvaluationsCustomers;
use App\Models\userImages;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\JWTAuthExceptions\JWTException;
use Hash;

/**
 * Description of UsersController
 *
 * @author Amna
 */
class UsersController extends Controller {

    public function index(Request $request) {
        $Users = new Users();
        if (isset($request->name)) {
            $Users = $Users->where('name', 'like', '%' . $request->name . '%');
        }
        if (isset($request->mobile)) {
            $Users = $Users->where('mobile', 'like', '%' . $request->mobile . '%');
        }
        if (isset($request->email)) {
            $Users = $Users->where('email', $request->email);
        }
        if (isset($request->status_id)) {
            $Users = $Users->where('status_id', $request->status_id);
        }
        return response()->json($Users->paginate(20));
    }

    public function store(Request $request) {
        //$data = json_decode($request->getContent(), true);
        $data = $request->toArray();
        $data['status_id'] = 1;
        if ($data['password'] == "" || $data['name'] == "" || $data['mobile'] == "" || $data['city'] == "") {
            $response["code"] = -2;
            $response['result_num'] = 0;
            $response["result_msg"] = 'يجب إدخال جميع الحقول المطلوبة';
            $response['result_object'] = [];
            return response()->json($response);
        }
        $usersCount = Users::where('mobile', $data['mobile'])->count();
        if ($usersCount > 0) {
            $response["code"] = -3;
            $response['result_num'] = 0;
            $response["result_msg"] = 'رقم الجوال مدخل مسبقاُ ';
            $response['result_object'] = [];
            return response()->json($response);
        }

        if (isset($data['profile_image'])) {
            $extension = strtolower(pathinfo($_FILES['profile_image']['name'], PATHINFO_EXTENSION));
            $tmp = '/images/'
                    . '_'
                    . round(microtime(true) * 1000)
                    . '.'
                    . $extension;
            if (move_uploaded_file($_FILES['profile_image']['tmp_name'], base_path() . '/storage' . $tmp)) {
                $data['image'] = $tmp;
            }
        }

        $data['password'] = Hash::make($data['password']);
        $user = Users::create($data);
        if ($user) {
            unset($user->password);
            if (isset($data['images'])) {
                foreach ($_FILES['images']['name'] as $key => $file) {
                    //dd($_FILES['things_images']['tmp_name'][$key]);
                    $extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                    $tmp = '/images/'
                            . '_'
                            . round(microtime(true) * 1000)
                            . '.'
                            . $extension;
                    //$base2image->save(base_path() . '/storage'.$tmp);
                    if (move_uploaded_file($_FILES['images']['tmp_name'][$key], base_path() . '/storage' . $tmp)) {
                        $userImages['image'] = $tmp;
                        $userImages['user_id'] = $user->id;
                        userImages::create($userImages);
                    }
                }
            }


            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = 'تم الإضافة بنجاح';
            $response['result_object'] = [['id' => $user->id, 'name' => $user->name, 'mobile' => $user->mobile, 'city' => $user->city,
            'work_id' => $user->work_id, 'status_id' => $user->status_id, 'created_at' => $user->created_at, 'updated_at' => $user->updated_at]];
        } else {
            $response["code"] = -1;
            $response['result_num'] = 0;
            $response["result_msg"] = 'فشلت عملية الإضافة';
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function show($id) {
        $response['data'] = Users::with(['carriers.status', 'customers.status', 'CityName', 'Country'])->find($id);
        $response['status'] = ConstantsTable::where('type', 1)->get();
        $response['carriersPoints'] = Evaluations::where('user_id', $id)->sum('evaluation');
        $carriersCount = Evaluations::where('user_id', $id)->count();
        if ($carriersCount == 0)
            $response['carriersAvg'] = 0;
        else
            $response['carriersAvg'] = $response['carriersPoints'] / $carriersCount;

        $response['customersPoints'] = EvaluationsCustomers::where('user_id', $id)->sum('evaluation');
        $customersCount = EvaluationsCustomers::where('user_id', $id)->count();
        if ($customersCount == 0)
            $response['customersAvg'] = 0;
        else
            $response['customersAvg'] = $response['customersPoints'] / $customersCount;
        return response()->json($response);
    }

    public function updateProfile(Request $request) {
        $data = $request->toArray();
        \Config::set('jwt.user', 'App\Models\Users');
        \Config::set('auth.providers.users.table', 'users');

        try {

            if (!$userAuth = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());
        }
        //$userAuth = JWTAuth::parseToken()->authenticate();
        // dd($userAuth);
        $id = $userAuth->id;
        //dd($id);
        $user = Users::with(['CityName', 'Country', 'Work'])->find($id);
        if ($user) {
            if (isset($data['profile_image'])) {
                $extension = strtolower(pathinfo($_FILES['profile_image']['name'], PATHINFO_EXTENSION));
                $tmp = '/images/'
                        . '_'
                        . round(microtime(true) * 1000)
                        . '.'
                        . $extension;
                if (move_uploaded_file($_FILES['profile_image']['tmp_name'], base_path() . '/storage' . $tmp)) {
                    $user->image = $tmp;
                }
            }

            if (isset($data['images'])) {
                userImages::where('user_id', $user->id)->delete();
                foreach ($_FILES['images']['name'] as $key => $file) {
                    //dd($_FILES['things_images']['tmp_name'][$key]);
                    $extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                    $tmp = '/images/'
                            . '_'
                            . round(microtime(true) * 1000)
                            . '.'
                            . $extension;
                    //$base2image->save(base_path() . '/storage'.$tmp);
                    if (move_uploaded_file($_FILES['images']['tmp_name'][$key], base_path() . '/storage' . $tmp)) {
                        $userImages['image'] = $tmp;
                        $userImages['user_id'] = $user->id;
                        userImages::create($userImages);
                    }
                }
            }

            $user->name = $data['name'];
            $user->mobile = $data['mobile'];
            $user->work_id = $data['work_id'];
            $user->city = $data['city'];
            $user->save();
            unset($user->password);

            if ($user->image != null) {
                $storage = storage_path(ltrim($user->image, '/'));
                $storage = explode('public_html', $storage);
                $user->image = 'http://' . $request->getHttpHost() . $storage[1];
            } else {
                $user->image = null;
            }

            $images = userImages::where('user_id', $user->id)->get();
            if ($images) {
                foreach ($images as $image) {
                    $storage = storage_path(ltrim($image->image, '/'));
                    $storage = explode('public_html', $storage);
                    $image->image = 'http://' . $request->getHttpHost() . $storage[1];
                }
                $user->images = $images;
            } else {
                $user->images = null;
            }

            if ($user->work_id != null) {
                $work = ConstantsTable::find($user->work_id);
                $workName = $work->name;
            } else {
                $workName = null;
            }

            if ($user->city != null) {
                $city = \App\Models\City::where('CityId', $user->city)->first();
                $cityName = $city->Name;
            } else {
                $cityName = null;
            }

            if ($user->country != null) {
                $country = \App\Models\Country::where('CountryId', $user->country)->first();
                $countryName = $country->Name;
            } else {
                $countryName = null;
            }



            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = 'تم التعديل بنجاح';
            //$response['result_object'] = $user;
            $response['result_object'] = [['id' => $user->id, 'name' => $user->name, 'mobile' => $user->mobile,
            'email' => $user->email, 'profile_image' => $user->image,
            'images' => $images,
            'work_id' => $user->work_id,
            'work_name' => $workName,
            'city_id' => $user->city,
            'city_name' => $cityName,
            'country_id' => $user->country,
            'country_name' => $countryName]];
        } else {
            $response["code"] = -1;
            $response['result_num'] = 0;
            $response["result_msg"] = 'فشلت عملية التعديل';
            $response['result_object'] = [];
        }

        return response()->json($response);
    }

    public function profile(Request $request) {
        \Config::set('jwt.user', 'App\Models\Users');
        \Config::set('auth.providers.users.table', 'users');

        try {

            if (!$userAuth = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());
        }
        //$userAuth = JWTAuth::parseToken()->authenticate();
        // dd($userAuth);
        $id = $userAuth->id;
        $user = Users::with(['CityName', 'Country', 'Work'])->find($id);

        if ($user) {
            $carriersPoints = Evaluations::where('user_id', $id)->sum('evaluation');
            $customersPoints = EvaluationsCustomers::where('user_id', $id)->sum('evaluation');

            $carriersCount = Evaluations::where('user_id', $id)->count();
            if ($carriersCount == 0)
                $carriersAvg = 0;
            else
                $carriersAvg = $carriersPoints / $carriersCount;

            $customersCount = EvaluationsCustomers::where('user_id', $id)->count();
            if ($customersCount == 0)
                $customersAvg = 0;
            else
                $customersAvg = $customersPoints / $customersCount;

            $images = userImages::where('user_id', $user->id)->get();
            foreach ($images as $image) {
                $storage = storage_path(ltrim($image->image, '/'));
                $storage = explode('public_html', $storage);
                $image->image = 'http://' . $request->getHttpHost() . $storage[1];
            }

            if ($user->image != null) {
                $storage = storage_path(ltrim($user->image, '/'));
                $storage = explode('public_html', $storage);
                $profileImg = 'http://' . $request->getHttpHost() . $storage[1];
            } else {
                $profileImg = null;
            }

            if ($user->Work != null) {
                $workName = $user->Work->name;
            } else {
                $workName = null;
            }

            if ($user->CityName != null) {
                $cityName = $user->CityName->Name;
            } else {
                $cityName = null;
            }

            if ($user->Country != null) {
                $countryName = $user->Country->Name;
            } else {
                $countryName = null;
            }

            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = "تم العثور على البيانات المطلوبة";
            $response['result_object'] = [['id' => $user->id, 'name' => $user->name, 'mobile' => $user->mobile,
            'email' => $user->email, 'profile_image' => $profileImg,
            'images' => $images,
            'work_id' => $user->work_id,
            'work_name' => $workName,
            'city_id' => $user->city,
            'city_name' => $cityName,
            'country_id' => $user->country,
            'country_name' => $countryName,
            'points as carrier' => (int) $carriersPoints,
            'evaluate as carrier' => (int) $carriersAvg,
            'points as customer' => (int) $customersPoints,
            'evaluate as customer' => (int) $customersAvg]];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function updatePassword(Request $request) {
        $data = $request->toArray();
        \Config::set('jwt.user', 'App\Models\Users');
        \Config::set('auth.providers.users.table', 'users');

        try {

            if (!$userAuth = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());
        }
        //$userAuth = JWTAuth::parseToken()->authenticate();
        // dd($userAuth);
        $id = $userAuth->id;
        $user = Users::with(['CityName', 'Country', 'Work'])->find($id);

        if ($user) {
            $data['password'] = Hash::make($data['password']);
            $user->password = $data['password'];
            $user->save();
            unset($user->password);
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = "تم تعديل كلمة المرور بنجاح";
            $response['result_object'] = [$user];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "المستخدم غير موجود";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function update(Request $request, $id) {
        $data = $request->data;
        $type = Users::find($id);

        $type->status_id = $data['status_id'];
        $type->delete_reason = $data['delete_reason'];

        $type->save();
        $response["status"] = true;
        $response["message"] = 'تم التعديل بنجاح';
        return response()->json($response);
    }

    public function login(Request $request) {

        \Config::set('jwt.user', 'App\Models\Users');
        \Config::set('auth.providers.users.table', 'users');

        $credentials = $request->only('mobile', 'password');


        try {
            // verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                $response["code"] = -1;
                $response['result_num'] = 0;
                $response["result_msg"] = 'اسم المستخدم أو كلمة المرور خطأ';
                $response['result_object'] = [];
                return response()->json($response, 200);
            }
        } catch (JWTException $e) {
            // something went wrong
            $response["code"] = -2;
            $response['result_num'] = 0;
            $response["result_msg"] = 'حدث خطأ ما، لم يتم انشاء التوكن';
            $response['result_object'] = [];
            return response()->json($response, 200);
        }

        $user = Users::where('mobile', $request->mobile)->first();
        unset($user->password);
        if ($user->image != null) {
            $storage = storage_path(ltrim($user->image, '/'));
            $storage = explode('public_html', $storage);
            $user->image = 'http://' . $request->getHttpHost() . $storage[1];
        }

//dd(JWTAuth::attempt($credentials));
        // if no errors are encountered we can return a JWT
        //return response()->json(compact('token'));
        $response["code"] = 1;
        $response['result_num'] = 1;
        $response["result_msg"] = 'تم تسجيل الدخول بنجاح';
        $response['result_object'] = [['id' => $user->id, 'name' => $user->name, 'mobile' => $user->mobile, 'email' => $user->email,
        'city' => $user->city, 'country' => $user->country, 'status_id' => $user->status_id, 'delete_reason' => $user->delete_reason,
        'work_id' => $user->work_id, 'image' => $user->image, 'created_at' => $user->created_at, 'updated_at' => $user->updated_at, 'token' => $token]];
        return response()->json($response);
    }

    public function forgetPassword(Request $request) {
        $data = $request->toArray();

        $user = Users::with(['CityName', 'Country', 'Work'])->where('mobile', $data['mobile'])->first();

        if ($user) {
            unset($user->password);
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = "سيتم ارسال الكود الي الجوال";
            $response['result_object'] = [$user];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "المستخدم غير موجود";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function resetPassword(Request $request) {
        $data = $request->toArray();

        if ($data['code'] != 000000) {
            $response['code'] = -2;
            $response['result_num'] = 0;
            $response['result_msg'] = "الكود المستخدم غير صحيح";
            $response['result_object'] = [];
            return response()->json($response);
        }

        $user = Users::with(['CityName', 'Country', 'Work'])->where('mobile', $data['mobile'])->first();

        if ($user) {
            $data['password'] = Hash::make($data['password']);
            $user->password = $data['password'];
            $user->save();
            unset($user->password);
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = "تم تعديل كلمة المرور بنجاح";
            $response['result_object'] = [$user];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "المستخدم غير موجود";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

}
