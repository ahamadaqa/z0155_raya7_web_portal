<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Notifications;
use Illuminate\Http\Request;

/**
 * Description of NotificationsController
 *
 * @author Amna
 */
class NotificationsController extends Controller {
    public function index(Request $request) {
        $Notifications = Notifications::with('Admin');
        if (isset($request->name)) {
            $Notifications = $Notifications->whereHas('Admin', function($x) use ($request) {
                $x->where('name', 'like', '%' . $request->name . '%');
            });
        }
        if (isset($request->from_date)) {
            $Notifications = $Notifications->whereDate('date', '>=',  $request->from_date );
        }
        if (isset($request->to_date)) {
            $Notifications = $Notifications->whereDate('date', '<=', $request->to_date);
        }
        return response()->json($Notifications->paginate(10));
    }
    
        public function store(Request $request) {
            $data['notify_text']=$request->data;
            $data['admin_id']=1;
            $data['date']=date('Y-m-d');
        Notifications::create($data);
        $response["status"] = true;
        $response["message"] = 'تم الإضافة بنجاح';
        return response()->json($response);
    }
    
    public function destroy($id) {
        Notifications::destroy($id);
        $response["status"] = true;
        $response["message"] = 'تم الحذف بنجاح';
        return response()->json($response);
    }

}
