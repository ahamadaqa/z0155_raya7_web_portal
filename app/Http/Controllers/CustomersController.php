<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Customers;
use App\Models\CustomersOrders;
use App\Models\customerTourImages;
use App\Models\CustomerTourRepeats;
use App\Models\Evaluations;
use App\Models\Brands;
use App\Models\ConstantsTable;
use App\Models\NotifyMe;
use Illuminate\Http\Request;
use App\Models\EvaluationsCustomers;
use JWTAuth;
use DB;

/**
 * Description of CustomersController
 *
 * @author Amna
 */
class CustomersController extends Controller {

    function __construct() {
        \Config::set('jwt.user', 'App\Models\Users');
        \Config::set('auth.providers.users.table', 'users');
    }

    public function index(Request $request) {
        $Customers = Customers::with(['User', 'Status']);
        if (isset($request->name)) {
            $Customers = $Customers->whereHas('User', function($x) use ($request) {
                $x->where('name', 'like', '%' . $request->name . '%');
            });
        }
        if (isset($request->city)) {
            $Customers = $Customers->where(function($x) use ($request) {
                $x->where('from_place', 'like', '%' . $request->city . '%');
                $x->orWhere('to_place', 'like', '%' . $request->city . '%');
            });
        }
        if (isset($request->status_id)) {
            $Customers = $Customers->where('status_id', $request->status_id);
        }
        return response()->json($Customers->paginate(20));
    }

    public function search(Request $request) {
        $data = json_decode($request->getContent(), true);
//dd($data);
        $Customers = Customers::with(['User', 'Status']);
        if (isset($data['transfer_way_id'])) {
            $Customers = $Customers->where('transfer_way_id', $data['transfer_way_id']);
        }
        if (isset($data['car_brand_id'])) {
            $Customers = $Customers->where('car_brand_id', $data['car_brand_id']);
        }
        if (isset($data['people_count'])) {
            $Customers = $Customers->where('person_count', $data['people_count']);
        }
        if (isset($data['men_count'])) {
            $Customers = $Customers->where('men_count', $data['men_count']);
        }
        if (isset($data['women_count'])) {
            $Customers = $Customers->where('women_count', $data['women_count']);
        }
        if (isset($data['children_count'])) {
            $Customers = $Customers->where('children_count', $data['children_count']);
        }
        if (isset($data['babies_count'])) {
            $Customers = $Customers->where('babies_count', $data['babies_count']);
        }
        if (isset($data['search_type'])) {
            if ($data['search_type'] == 1 || $data['search_type'] == 4) {
                $Customers = $Customers->where('person_count', '>', 0);
                $Customers = $Customers->where('things_count', '>', 0);
            } elseif ($data['search_type'] == 2) {
                $Customers = $Customers->where('things_count', '>', 0);
            } elseif ($data['search_type'] == 3) {
                $Customers = $Customers->where('person_count', '>', 0);
            }
        }

        if (isset($data['from_date'])) {
            $Customers = $Customers->where(DB::raw('DATE_SUB(DATE_SUB(DATE_ADD(DATE_ADD(from_date, INTERVAL flexible_days_after DAY), INTERVAL flexible_minutes_after MINUTE), INTERVAL flexible_days_before DAY), INTERVAL flexible_minutes_before MINUTE)')
                    , '>=', $data['from_date']);
//            $Customers = $Customers->Where(DB::raw('DATE_ADD(DATE_ADD(from_date, INTERVAL flexible_days_after DAY), INTERVAL flexible_minutes_after MINUTE)')
//                    , '>=', $data['from_date']);
        }
        if (isset($data['to_date'])) {
            $Customers = $Customers->where('to_date', '<=', $data['to_date']);
        }

        if (isset($data['orderBy'])) {
            if ($data['orderBy'] == "points") {
                $Customers = $Customers->orderBy('distance', 'desc');
            } elseif ($data['orderBy'] == "distance") {
                $Customers = $Customers->orderBy('distance', 'desc');
            } elseif ($data['orderBy'] == "evaluation") {
                $Customers = $Customers->orderBy('distance', 'desc');
            } elseif ($data['orderBy'] == "price") {
                $Customers = $Customers->orderBy('price', 'asc');
            }
            //$Customers = $Customers->where('to_date', "<=", $data['to_date']);
        }

        $result = $Customers->paginate($request['per_page']);
        $datadata = $result->toArray();

        if (isset($data['from_latitude']) && isset($data['from_longitude'])) {
            foreach ($datadata['data'] as $key => $res) {
                $mainDistance = Customers::distance($data['from_latitude'], $data['from_longitude'], $res['from_point_x'], $res['to_point_x'], "K");

                if ($mainDistance > 1) {
                    unset($datadata['data'][$key]);
                }
            }
        }

        if (isset($data['to_latitude']) && isset($data['to_longitude'])) {
            foreach ($datadata['data'] as $key => $res) {
                $mainDistance2 = Customers::distance($data['to_latitude'], $data['to_longitude'], $res['from_point_y'], $res['to_point_y'], "K");

                if ($mainDistance2 > 1) {
                    unset($datadata['data'][$key]);
                }
            }
        }



        $datadata['data'] = array_values($datadata['data']);

        foreach ($datadata['data'] as $key => $res) {
            $datadata['data'][$key]['track'] = json_decode($res['track'], true);
            $datadata['data'][$key]['status_id'] = (int) $res['status_id'];
            $datadata['data'][$key]['distance'] = (int) $res['distance'];
            unset($datadata['data'][$key]['user']['password']);

            if ($datadata['data'][$key]['user']['image'] != null) {
                $storage = storage_path(ltrim($datadata['data'][$key]['user']['image'], '/'));
                $storage = explode('public_html', $storage);
                $datadata['data'][$key]['user']['image'] = 'http://' . $request->getHttpHost() . $storage[1];
            }

            if ($datadata['data'][$key]['user'] != null) {
                $user = $datadata['data'][$key]['user'];
                $carriersPoints = Evaluations::where('user_id', $user['id'])->sum('evaluation');
                $carriersCount = Evaluations::where('user_id', $user['id'])->count();
                if ($carriersCount == 0)
                    $carriersAvg = 0;
                else
                    $carriersAvg = $carriersPoints / $carriersCount;

                $customersPoints = EvaluationsCustomers::where('user_id', $user['id'])->sum('evaluation');
                $customersCount = EvaluationsCustomers::where('user_id', $user['id'])->count();
                if ($customersCount == 0)
                    $customersAvg = 0;
                else
                    $customersAvg = $customersPoints / $customersCount;

                $datadata['data'][$key]['user']['rating_average'] = $carriersAvg + $customersAvg;
                $datadata['data'][$key]['user']['rating_count'] = $carriersCount + $customersCount;
            }
        }

        $count = count($datadata['data']);
        if ($count > 0) {
            $response['code'] = 1;
            $response['result_num'] = $count;
            $response['result_msg'] = 'تم العثور على البيانات المطلوبة';
            $response['result_object'] = [$datadata];
        } else {
            $response['code'] = -1;
            $response['result_num'] = $count;
            $response['result_msg'] = 'لا يوجد نتائج';
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function store(Request $request) {
        $data = $request->toArray();
//dd($data);
        DB::beginTransaction();

        $user = JWTAuth::parseToken()->authenticate();
//dd($user);
        $data['user_id'] = $user->id;

        $data['from_point_x'] = $data['from_latitude'];
        $data['to_point_x'] = $data['from_longitude'];
        $data['from_point_y'] = $data['to_latitude'];
        $data['to_point_y'] = $data['to_longitude'];
        $data['date'] = date('Y-m-d');
        $data['status_id'] = 4;

        if ((!isset($data['from_latitude']) && $data['from_latitude'] == '') ||
                (!isset($data['from_longitude']) && $data['from_longitude'] == '') ||
                (!isset($data['to_latitude']) && $data['to_latitude'] == '') ||
                (!isset($data['to_longitude']) && $data['to_longitude'] == '') ||
                (!isset($data['from_date']) && $data['from_date'] == '') ||
                (!isset($data['to_date']) && $data['to_date'] == '')) {
            $response["code"] = -2;
            $response['result_num'] = 0;
            $response["result_msg"] = 'يجب ادخال جميع الحقول المطلوبة';
            $response['result_object'] = [];
            return response()->json($response);
        }

        if ($data['from_date'] < date('Y-m-d')) {
            $response["code"] = -3;
            $response['result_num'] = 0;
            $response["result_msg"] = 'يجب أن يكون تاريخ بداية الرحلة أكبر او يساوي تاريخ اليوم';
            $response['result_object'] = [];
            return response()->json($response);
        }

        if ($data['to_date'] < $data['from_date']) {
            $response["code"] = -4;
            $response['result_num'] = 0;
            $response["result_msg"] = 'يجب أن يكون تاريخ نهاية الرحلة أكبر او يساوي تاريخ بداية الرحلة';
            $response['result_object'] = [];
            return response()->json($response);
        }

        if ($data['person_count'] < ($data['men_count'] + $data['women_count'] + $data['children_count'] + $data['babies_count'])) {
            $response["code"] = -5;
            $response['result_num'] = 0;
            $response["result_msg"] = 'يجب أن يكون عدد الأشخاص أكبر أو يساوي مجموع التفاصيل';
            $response['result_object'] = [];
            return response()->json($response);
        }

        $track = json_decode($data['track'], true);
        if ($track == null) {
            $response["code"] = -6;
            $response['result_num'] = 0;
            $response["result_msg"] = 'صيغة المسار غير صحيحة، يجب كتابتها بشكل صحيح';
            $response['result_object'] = [];
            return response()->json($response);
        }

        $customerTour = Customers::orderBy('order_id', 'desc')->first();
        if ($customerTour)
            $data['order_id'] = $customerTour->order_id + 1;
        else
            $data['order_id'] = 1;

        $distance = Customers::distance($data['from_latitude'], $data['from_longitude'], $data['to_latitude'], $data['to_longitude'], "K");
        $data['distance'] = round($distance, 2);

        $tour = Customers::create($data);


        if (isset($data['things_images'])) {
            foreach ($_FILES['things_images']['name'] as $key => $file) {
//dd($_FILES['things_images']['tmp_name'][$key]);
                $extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                $tmp = '/images/'
                        . '_'
                        . round(microtime(true) * 1000)
                        . '.'
                        . $extension;
//$base2image->save(base_path() . '/storage'.$tmp);
                if (move_uploaded_file($_FILES['things_images']['tmp_name'][$key], base_path() . '/storage' . $tmp)) {
                    $image['tour_id'] = $tour->id;
                    $image['type'] = 1;
                    $image['image'] = $tmp;

                    customerTourImages::create($image);
                }
            }
        }
        if (isset($data['card_image'])) {

            foreach ($_FILES['card_image']['name'] as $key => $file) {
                $extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                $tmp = '/images/'
                        . '_'
                        . round(microtime(true) * 1000)
                        . '.'
                        . $extension;
//$base2image->save(base_path() . '/storage'.$tmp);
                if (move_uploaded_file($_FILES['card_image']["tmp_name"][$key], base_path() . '/storage' . $tmp)) {
                    $image['tour_id'] = $tour->id;
                    $image['type'] = 2;
                    $image['image'] = $tmp;

                    customerTourImages::create($image);
                }
            }
        }

        if ($data['want_repeat'] == 1) {
            $repeats = $data['repeats'];
            $repeats['tour_id'] = $tour->id;
            if ($repeats['end_date'] <= date('Y-m-d')) {
                $response["code"] = -7;
                $response['result_num'] = 0;
                $response["result_msg"] = 'يجب أن يكون تاريخ نهاية التكرار أكبر من تاريخ اليوم';
                $response['result_object'] = [];
                return response()->json($response);
            }

            if ($repeats['end_date'] <= $data['from_date']) {
                $response["code"] = -8;
                $response['result_num'] = 0;
                $response["result_msg"] = 'يجب أن يكون تاريخ نهاية التكرار أكبر من تاريخ بداية الرحلة';
                $response['result_object'] = [];
                return response()->json($response);
            }

            CustomerTourRepeats::create($repeats);
        }

        $tour->track = json_decode($tour->track, true);
        $tour->status_id = (int) $tour->status_id;
        $tour->distance = (int) $tour->distance;

        DB::commit();

        if ($tour) {
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = 'تم الإضافة بنجاح';
            $response['result_object'] = [$tour];
        } else {
            $response["code"] = -1;
            $response['result_num'] = 0;
            $response["result_msg"] = 'حدث خطأ ما ولم يتم الحفظ';
            ;
            $response['result_object'] = [];
        }

        return response()->json($response);
    }

    public function update($id, Request $request) {
        $data = $request->toArray();
        DB::beginTransaction();
        $user = JWTAuth::parseToken()->authenticate();
        $trip = Customers::with(['User', 'Status'])->find($id);
        if ($trip) {
            if ($trip->status_id != 4) {
                $response['code'] = -1;
                $response['result_num'] = 0;
                $response['result_msg'] = "لا يمكنك تعديل طلب رحلة حالته ليست جديدة";
                $response['result_object'] = [];
                return response()->json($response);
            }
            $orders = CustomersOrders::where('customer_tour_id', $trip->id)->count();
            if ($orders > 0) {
                $response['code'] = -2;
                $response['result_num'] = 0;
                $response['result_msg'] = "لا يمكنك تعديل طلب رحلة تم عمل طلب مشاركة عليه";
                $response['result_object'] = [];
                return response()->json($response);
            }
            $data['from_point_x'] = $data['from_latitude'];
            $data['to_point_x'] = $data['from_longitude'];
            $data['from_point_y'] = $data['to_latitude'];
            $data['to_point_y'] = $data['to_longitude'];
            $data['person_count'] = $data['person_count'];
            if ($user->id != $trip->user_id) {
                $response['code'] = -3;
                $response['result_num'] = 0;
                $response['result_msg'] = "لا يمكنك تعديل طلب رحلة لم تقم بإضافته";
                $response['result_object'] = [];
                return response()->json($response);
            }
            if ((!isset($data['from_latitude']) && $data['from_latitude'] == '') ||
                    (!isset($data['from_longitude']) && $data['from_longitude'] == '') ||
                    (!isset($data['to_latitude']) && $data['to_latitude'] == '') ||
                    (!isset($data['to_longitude']) && $data['to_longitude'] == '') ||
                    (!isset($data['from_date']) && $data['from_date'] == '') ||
                    (!isset($data['to_date']) && $data['to_date'] == '') ||
                    (!isset($data['transfer_way_id']) && $data['transfer_way_id'] == '')) {
                $response["code"] = -4;
                $response['result_num'] = 0;
                $response["result_msg"] = 'يجب ادخال جميع الحقول المطلوبة';
                $response['result_object'] = [];
                return response()->json($response);
            }

            if ($data['person_count'] < ($data['men_count'] + $data['women_count'] + $data['children_count'] + $data['babies_count'])) {
                $response["code"] = -5;
                $response['result_num'] = 0;
                $response["result_msg"] = 'يجب أن يكون عدد الأشخاص أكبر أو يساوي مجموع التفاصيل';
                $response['result_object'] = [];
                return response()->json($response);
            }
            $distance = Customers::distance($data['from_latitude'], $data['from_longitude'], $data['to_latitude'], $data['to_longitude'], "K");
            $data['distance'] = round($distance, 2);
//        if ($data['from_date'] < date('Y-m-d')) {
//            $response["code"] = -6;
//            $response['result_num'] = 0;
//            $response["result_msg"] = 'يجب أن يكون تاريخ بداية الرحلة أكبر او يساوي تاريخ اليوم';
//            $response['result_object'] = '';
//            return response()->json($response);
//        }

            if ($data['to_date'] < $data['from_date']) {
                $response["code"] = -6;
                $response['result_num'] = 0;
                $response["result_msg"] = 'يجب أن يكون تاريخ نهاية الرحلة أكبر او يساوي تاريخ بداية الرحلة';
                $response['result_object'] = [];
                return response()->json($response);
            }

            $track = json_decode($data['track'], true);
            if ($track == null) {
                $response["code"] = -7;
                $response['result_num'] = 0;
                $response["result_msg"] = 'صيغة المسار غير صحيحة، يجب كتابتها بشكل صحيح';
                $response['result_object'] = [];
                return response()->json($response);
            }

            $trip->update($data);

            if (isset($data['things_images'])) {
                customerTourImages::where('tour_id', $trip->id)->where('type', 1)->delete();
                foreach ($_FILES['things_images']['name'] as $key => $file) {
//dd($_FILES['things_images']['tmp_name'][$key]);
                    $extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                    $tmp = '/images/'
                            . '_'
                            . round(microtime(true) * 1000)
                            . '.'
                            . $extension;
//$base2image->save(base_path() . '/storage'.$tmp);
                    if (move_uploaded_file($_FILES['things_images']['tmp_name'][$key], base_path() . '/storage' . $tmp)) {
                        $image['tour_id'] = $trip->id;
                        $image['type'] = 1;
                        $image['image'] = $tmp;

                        customerTourImages::create($image);
                    }
                }
            }

            if (isset($data['card_image'])) {
                customerTourImages::where('tour_id', $trip->id)->where('type', 2)->delete();
                foreach ($_FILES['card_image']['name'] as $key => $file) {
                    $extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                    $tmp = '/images/'
                            . '_'
                            . round(microtime(true) * 1000)
                            . '.'
                            . $extension;

                    if (move_uploaded_file($_FILES['card_image']["tmp_name"][$key], base_path() . '/storage' . $tmp)) {
                        $image['tour_id'] = $trip->id;
                        $image['type'] = 2;
                        $image['image'] = $tmp;

                        customerTourImages::create($image);
                    }
                }
            }

            if ($data['want_repeat'] == 1) {
                CustomerTourRepeats::where('tour_id', $trip->id)->delete();
                $repeats = $data['repeats'];
                $repeats['tour_id'] = $trip->id;
                if ($repeats['end_date'] <= date('Y-m-d')) {
                    $response["code"] = -8;
                    $response['result_num'] = 0;
                    $response["result_msg"] = 'يجب أن يكون تاريخ نهاية التكرار أكبر من تاريخ اليوم';
                    $response['result_object'] = [];
                    return response()->json($response);
                }
                if ($repeats['end_date'] <= $data['from_date']) {
                    $response["code"] = -9;
                    $response['result_num'] = 0;
                    $response["result_msg"] = 'يجب أن يكون تاريخ نهاية التكرار أكبر من تاريخ بداية الرحلة';
                    $response['result_object'] = [];
                    return response()->json($response);
                }
                CustomerTourRepeats::create($repeats);
            }

            $trip->track = json_decode($trip->track, true);
            $trip->status_id = (int) $trip->status_id;
            $trip->distance = (int) $trip->distance;
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = 'تم التعديل بنجاح';
            $response['result_object'] = [$trip];
        } else {
            $response['code'] = -10;
            $response['result_num'] = 0;
            $response['result_msg'] = "طلب الرحلة غير موجود";
            $response['result_object'] = [];
        }
        DB::commit();


        return response()->json($response);
    }

    public function getTrip(Request $request, $id) {
        $trip = Customers::with(['User', 'Status'])->find($id);
        if ($trip) {
            unset($trip->User->password);
            $trip->order_id = str_pad($trip->order_id, 9, '0', STR_PAD_LEFT);
            $trip->track = json_decode($trip->track, true);
            $brands = Brands::where('BrandId', $trip->car_brand_id)->first();
            if ($brands)
                $trip->car_brand_name = $brands->Name;
            $transferWays = ConstantsTable::find($trip->transfer_way_id);
            if ($transferWays)
                $trip->transfer_way_name = $transferWays->name;

            $images = customerTourImages::where('tour_id', $id)->where('type', 1)->get();
//            dd($images);
            if ($images) {
                foreach ($images as $image) {
                    $storage = storage_path(ltrim($image->image, '/'));
                    $storage = explode('public_html', $storage);
                    $image->image = 'http://' . $request->getHttpHost() . $storage[1];
                }

                $trip->things_images = $images;
            } else {
                $trip->things_images = '';
            }

            $Cimages = customerTourImages::where('tour_id', $id)->where('type', 2)->get();
//            dd($images);
            if ($Cimages) {
                foreach ($Cimages as $image) {
                    $storage = storage_path(ltrim($image->image, '/'));
                    $storage = explode('public_html', $storage);
                    $image->image = 'http://' . $request->getHttpHost() . $storage[1];
                }

                $trip->card_images = $Cimages;
            } else {
                $trip->card_images = '';
            }

            if (isset($trip->User->image) && ($trip->User->image != null || $trip->User->image != '')) {
                $storage = storage_path(ltrim($trip->User->image, '/'));
                $storage = explode('public_html', $storage);
                $trip->User->image = 'http://' . $request->getHttpHost() . $storage[1];
            }

            if (isset($trip->User->city) && ($trip->User->city != null || $trip->User->city != '')) {
                $city = \App\Models\City::where('CityId', $trip->User->city)->first();
                if ($city)
                    $trip->User->city_name = $city->Name;
            }

            if (isset($trip->User->country) && ($trip->User->country != null || $trip->User->country != '')) {
                $country = \App\Models\Country::where('CountryId', $trip->User->country)->first();
                if ($country)
                    $trip->User->country_name = $country->Name;
            }
            if (isset($trip->User->work_id) && ($trip->User->work_id != null || $trip->User->work_id != '')) {
                $work = ConstantsTable::find($trip->User->work_id);
                if ($work)
                    $trip->User->work_name = $work->name;
            }

            $trip->status_id = (int) $trip->status_id;
            $trip->distance = (int) $trip->distance;
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = "تم العثور على البيانات المطلوبة";
            $response['result_object'] = [$trip];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }

        return response()->json($response);
    }

    public function shareTrip(Request $request) {
        $data = json_decode($request->getContent(), TRUE);
        $trip = Customers::find($data['customer_tour_id']);
        if (!$trip) {
            $response["code"] = -3;
            $response['result_num'] = 0;
            $response["result_msg"] = 'الرحلة غير موجودة، يرجى التأكد من البيانات المرسلة!';
            $response['result_object'] = [];
            return response()->json($response);
        }
//        if ($data['price'] == "") {
//            $response["code"] = -2;
//            $response['result_num'] = 0;
//            $response["result_msg"] = 'السعر مطلوب';
//            $response['result_object'] = [];
//            return response()->json($response);
//        }
        $user = JWTAuth::parseToken()->authenticate();
        if ($user->id == $trip->user_id) {
            $response["code"] = -4;
            $response['result_num'] = 0;
            $response["result_msg"] = 'لايمكنك المشاركة برحتلك الخاصة';
            $response['result_object'] = [];
            return response()->json($response);
        }
        $candCount = CustomersOrders::where('status_id', 2)->where('customer_tour_id', $data['customer_tour_id'])->count();
        if ($candCount > 0) {
            $response['code'] = -6;
            $response['result_num'] = 0;
            $response['result_msg'] = "تم إغلاق المشاركة في هذه الرحلة !";
            $response['result_object'] = [];
            return response()->json($response);
        }
        $userOrder = CustomersOrders::where('user_id', $user->id)->where('customer_tour_id', $data['customer_tour_id'])
                        ->where('status_id', '!=', 3)->where('status_id', '!=', 4)->first();
        if ($userOrder) {
            $response["code"] = -5;
            $response['result_num'] = 0;
            $response["result_msg"] = 'يوجد طلب سابق لنفس الرحلة !';
            $response['result_object'] = [];
            return response()->json($response);
        }

        $data['user_id'] = $user->id;
        $data['status_id'] = 1;
        
        $distance = Customers::distance($trip->from_point_x, $trip->to_point_x, $data['latitude'], $data['longitude'], "K");
        $data['distance'] = round($distance, 2);
        
        $carrierOrder = CustomersOrders::create($data);
        if ($carrierOrder) {
            $carrierOrder->status_id = (int) $carrierOrder->status_id;
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = 'تم مشاركتك بالرحلة بنجاح';
            $response['result_object'] = [$carrierOrder];
        } else {
            $response["code"] = -1;
            $response['result_num'] = 0;
            $response["result_msg"] = 'فشلت مشاركتك بالرحلة ، حدث مشكلة يرجى المحاولة مرة أخرى';
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function avilableTracks($id) {
        $candidates = CustomersOrders::with('User')->where('customer_tour_id', $id)->get();
        $candidatesCount = CustomersOrders::with('User')->where('customer_tour_id', $id)->count();
        if ($candidatesCount > 0) {
            $response["code"] = 1;
            $response['result_num'] = $candidatesCount;
            $response["result_msg"] = "تم العثور على النتائج المطلوبة";
            $response['result_object'] = $candidates;
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function evaluate($id, Request $request) {
        $data = json_decode($request->getContent(), TRUE);
        if ($data['points'] == "") {
            $response["code"] = -2;
            $response['result_num'] = 0;
            $response["result_msg"] = 'التقييم مطلوب';
            $response['result_object'] = [];
            return response()->json($response);
        }
        $data1['evaluation'] = $data['points'];
        $data1['customer_id'] = $data['trip_id'];
        $data1['notes'] = $data['notes'];
        $data1['user_id'] = $id;
        $data1['date'] = date("Y-m-d");
        
        $oldEvaluations = \App\Models\EvaluationsCustomers::where('customer_id', $data['trip_id'])
                ->where('user_id', $id)->count();
        if($oldEvaluations > 0){
            $response["code"] = -3;
            $response['result_num'] = 0;
            $response["result_msg"] = 'تم التقييم مسبقاَ';
            $response['result_object'] = []; 
            return response()->json($response);
        }

        $carrierOrder = \App\Models\EvaluationsCustomers::create($data1);
        if ($carrierOrder) {
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = 'تم التقييم بنجاح';
            $response['result_object'] = [$carrierOrder];
        } else {
            $response["code"] = -1;
            $response['result_num'] = 0;
            $response["result_msg"] = 'حدث مشكلة أثناء عملية التقييم، يرجى المحاولة مرة أخرى';
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function cancelTripCustomer(Request $request) {
        $data = json_decode($request->getContent(), TRUE);
        $trip = Customers::find($data['trip_id']);
        if ($trip) {
            $trip->status_id = $data['status_id'];
            $trip->save();
            $trip->status_id = (int) $trip->status_id;
            $trip->distance = (int) $trip->distance;
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = "تم الحفظ بنجاح ";
            $response['result_object'] = [$trip];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function constants($id) {
        if ($id) {
            if ($id == 5) {
                $myTable = \App\Models\Brands::get();
            } else if ($id == 6) {
                $myTable = \App\Models\City::get();
            } else if ($id == 7) {
                $myTable = \App\Models\Country::get();
            } else {
                $myTable = \App\Models\ConstantsTable::where('type', $id)->get();
            }
        } else {
            $myTable['transfer_type'] = \App\Models\ConstantsTable::where('type', 3)->get();
            $myTable['work_type'] = \App\Models\ConstantsTable::where('type', 4)->get();
            $myTable['car_brands'] = \App\Models\Brands::get();
            $myTable['cities'] = \App\Models\City::get();
            $myTable['countries'] = \App\Models\Country::get();
        }

        $response["code"] = 1;
        $response['result_num'] = count($myTable);
        $response["result_msg"] = "تم العثور على البيانات المطلوبة";
        $response['result_object'] = $myTable;
        return response()->json($response);
    }
    
    public function getCityByCountry($id) {
       $cities = \App\Models\City::where('CountryId', $id)->get(); 
       if(count($cities) > 0){
       $response["code"] = 1;
        $response['result_num'] = count($cities);
        $response["result_msg"] = "تم العثور على البيانات المطلوبة";
        $response['result_object'] = [$cities];
       } else {
       $response["code"] = -1;
        $response['result_num'] = 0;
        $response["result_msg"] = "لا توجد مدن تابعة لرقم الدولة";
        $response['result_object'] = [];    
       }
        return response()->json($response);
    }

    public function table(Request $request) {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $id = $userAuth->id;
        $myTable = new Customers();

        if (isset($request['date'])) {
            //dd($request['date']);
            $myTable = $myTable->whereDate('from_date', '=', $request['date']);
        }
        if (isset($request['status_id'])) {
            // dd($request['status_id']);
            $myTable = $myTable->where('status_id', $request['status_id']);
        }

        $myTable = $myTable->where('user_id', $id)
                ->orWhere(function($query) use ($id, $request) {
            if (isset($request['date'])) {
                $query->whereDate('from_date', '=', $request['date']);
            }
            if (isset($request['status_id'])) {
                $query->where('status_id', $request['status_id']);
            }
            $query->WhereHas('customersOrders', function( $q ) use ($id) {
                $q->where('user_id', $id)->where('status_id', '!=', 4);
            });
        });

        $myTable = $myTable->orderBy('id', 'desc')->paginate($request['per_page']);
        $count = count($myTable);
        if ($count > 0) {
            foreach ($myTable as $trip) {
                $trip->track = json_decode($trip->track, true);
                $trip->status_id = (int) $trip->status_id;
                $trip->distance = (int) $trip->distance;
            }
            $response["code"] = 1;
            $response['result_num'] = $count;
            $response["result_msg"] = "تم العثور على البيانات المطلوبة";
            $response['result_object'] = [$myTable];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }

        return response()->json($response);
    }

    public function candidates($id, Request $request) {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $userid = $userAuth->id;
        $candidates = CustomersOrders::with('User')->where('customer_tour_id', $id)->where('status_id', '!=', 4)
                ->where(function($query) use ($userid) {
                    $query->whereHas('TripReq', function($q) use ($userid) {
                        $q->where('user_id', $userid);
                    })
                    ->orWhere('user_id', $userid);
                })
                ->get();
        $candidatesCount = CustomersOrders::with('User')->where('customer_tour_id', $id)->where('status_id', '!=', 4)
                        ->where(function($query) use ($userid) {
                            $query->whereHas('TripReq', function($q) use ($userid) {
                                $q->where('user_id', $userid);
                            })
                            ->orWhere('user_id', $userid);
                        })->count();
        if ($candidatesCount > 0) {
            foreach ($candidates as $cand) {
                $carriersPoints = Evaluations::where('user_id', $cand->user_id)->sum('evaluation');
                $carriersCount = Evaluations::where('user_id', $cand->user_id)->count();
                if ($carriersCount == 0)
                    $carriersAvg = 0;
                else
                    $carriersAvg = $carriersPoints / $carriersCount;

                $customersPoints = EvaluationsCustomers::where('user_id', $cand->user_id)->sum('evaluation');
                $customersCount = EvaluationsCustomers::where('user_id', $cand->user_id)->count();
                if ($customersCount == 0)
                    $customersAvg = 0;
                else
                    $customersAvg = $customersPoints / $customersCount;

                $cand->rating_average = $carriersAvg + $customersAvg;
                $cand->rating_count = $carriersCount + $customersCount;

                unset($cand->user->password);
                if ($cand->user->image != null) {
                    $storage = storage_path(ltrim($cand->user->image, '/'));
                    $storage = explode('public_html', $storage);
                    $cand->user->image = 'http://' . $request->getHttpHost() . $storage[1];
                }
                $cand->status_id = (int) $cand->status_id;
            }
            $response["code"] = 1;
            $response['result_num'] = $candidatesCount;
            $response["result_msg"] = "تم العثور على النتائج المطلوبة";
            $response['result_object'] = $candidates;
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function viewCandidates($id, Request $request) {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $userid = $userAuth->id;
        $candidates = CustomersOrders::with('User', 'evaluates')
                        ->where(function($query) use ($userid) {
                            $query->whereHas('TripReq', function($q) use ($userid) {
                                $q->where('user_id', $userid);
                            })
                            ->orWhere('user_id', $userid);
                        })->find($id);
        if ($candidates) {
            $carriersPoints = Evaluations::where('user_id', $candidates->user_id)->sum('evaluation');
            $carriersCount = Evaluations::where('user_id', $candidates->user_id)->count();
            if ($carriersCount == 0)
                $carriersAvg = 0;
            else
                $carriersAvg = $carriersPoints / $carriersCount;

            $customersPoints = EvaluationsCustomers::where('user_id', $candidates->user_id)->sum('evaluation');
            $customersCount = EvaluationsCustomers::where('user_id', $candidates->user_id)->count();
            if ($customersCount == 0)
                $customersAvg = 0;
            else
                $customersAvg = $customersPoints / $customersCount;

            $candidates->rating_average = $carriersAvg + $customersAvg;
            $candidates->rating_count = $carriersCount + $customersCount;

            unset($candidates->user->password);
            if ($candidates->user->image != null) {
                $storage = storage_path(ltrim($candidates->user->image, '/'));
                $storage = explode('public_html', $storage);
                $candidates->user->image = 'http://' . $request->getHttpHost() . $storage[1];
            }
            $candidates->status_id = (int) $candidates->status_id;
            
//            $evaluate = EvaluationsCustomers::where('user_id', $candidates->user_id)->where('customer_id', $candidates->customer_tour_id)->first();
//            if($evaluate) {
//            $candidates->is_evaluate = 1;   
//            $candidates->evaluate_value = $evaluate->evaluation; 
//            } else {
//            $candidates->is_evaluate = 0;   
//            $candidates->evaluate_value = null;   
//            }

            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = "تم العثور على النتائج المطلوبة";
            $response['result_object'] = [$candidates];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function confirmOrCancel(Request $request) {
        $data = json_decode($request->getContent(), true);
        $trip = Customers::find($data['trip_request_id']);

        $userAuth = JWTAuth::parseToken()->authenticate();
        $uid = $userAuth->id;
        if ($trip) {
            if ($uid != $trip->user_id) {
                $response['code'] = -2;
                $response['result_num'] = 0;
                $response['result_msg'] = "غير مسموح لك بقبول أو رفض الطلب";
                $response['result_object'] = [];
                return response()->json($response);
            }
            $trip->status_id = $data['status_id'];
//            $trip->cancel_reason = $data['cancel_reason'];
            $trip->save();
            $trip->status_id = (int) $trip->status_id;
            $trip->distance = (int) $trip->distance;
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = "تم الحفظ بنجاح";
            $response['result_object'] = [$trip];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function acceptOrRefuseCandidates($id, Request $request) {
        $candidates = CustomersOrders::with('TripReq')->find($id);
        $data = json_decode($request->getContent(), true);

        if ($data['status_id'] != 2 && $data['status_id'] != 3) {
            $response['code'] = -2;
            $response['result_num'] = 0;
            $response['result_msg'] = "الحالة المدخلة غير صحيحة";
            $response['result_object'] = [];
            return response()->json($response);
        }

        $userAuth = JWTAuth::parseToken()->authenticate();
        $uid = $userAuth->id;
        if ($candidates) {
            if ($uid != $candidates->TripReq->user_id) {
                $response['code'] = -3;
                $response['result_num'] = 0;
                $response['result_msg'] = "غير مسموح لك بقبول أو رفض الطلب";
                $response['result_object'] = [];
                return response()->json($response);
            }

            $candCount = CustomersOrders::where('status_id', 2)->where('customer_tour_id', $candidates->TripReq->id)->count();
            if ($candCount > 0) {
                $response['code'] = -4;
                $response['result_num'] = 0;
                $response['result_msg'] = "تم إغلاق المشاركة في هذه الرحلة !";
                $response['result_object'] = [];
                return response()->json($response);
            }

            $candidates->status_id = $data['status_id'];
            $candidates->save();
            if ($data['status_id'] == 2) {
//                $trip = Customers::find($candidates->TripReq->id);
                $candidates->TripReq->status_id = 6;
                $candidates->TripReq->save();
            }

            $candidates->status_id = (int) $candidates->status_id;
            $candidates->TripReq->track = json_decode($candidates->TripReq->track, true);
            $candidates->TripReq->status_id = (int) $candidates->TripReq->status_id;
            $candidates->TripReq->distance = (int) $candidates->TripReq->distance;
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = "تم الحفظ بنجاح ";
            $response['result_object'] = [$candidates];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = " المشارك غير موجود";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function candidateCancelRequest($id) {
        $candidates = CustomersOrders::with('TripReq')->find($id);
        $userAuth = JWTAuth::parseToken()->authenticate();
        $uid = $userAuth->id;
        if ($candidates) {
            if ($uid != $candidates->user_id) {
                $response['code'] = -2;
                $response['result_num'] = 0;
                $response['result_msg'] = "غير مسموح لك بالغاء طلب ليس لك";
                $response['result_object'] = [];
                return response()->json($response);
            }
//            if ($candidates->status_id != 1) {
//                $response['code'] = -3;
//                $response['result_num'] = 0;
//                $response['result_msg'] = "غير مسموح لك بالغاء طلب بعد قبوله";
//                $response['result_object'] = [];
//                return response()->json($response);
//            }

            $candidates->status_id = 4;
            $candidates->save();
            if($candidates->TripReq->status_id == 6){
              $candidates->TripReq->status_id = 5;
              $candidates->TripReq->save();
            }
            $candidates->status_id = (int) $candidates->status_id;
            $candidates->TripReq->track = json_decode($candidates->TripReq->track, true);
            $candidates->TripReq->status_id = (int) $candidates->TripReq->status_id;
            $candidates->TripReq->distance = (int) $candidates->TripReq->distance;
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = "تم إلغاء مشاركتك بنجاح ";
            $response['result_object'] = [$candidates];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }

        return response()->json($response);
    }

    public function notifyMe(Request $request) {
        $data = json_decode($request->getContent(), true);

        $userAuth = JWTAuth::parseToken()->authenticate();
        $uid = $userAuth->id;
        if (!$uid) {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "عليك تسجيل الدخول أولاً";
            $response['result_object'] = [];
            return response()->json($response);
        }

        $data['type'] = 2;
        $data['user_id'] = $uid;

        $notif = NotifyMe::create($data);
        if ($notif) {
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = "سيتم إرسال إشعار لك";
            $response['result_object'] = [];
            return response()->json($response);
        } else {
            $response['code'] = -2;
            $response['result_num'] = 0;
            $response['result_msg'] = "حدث خطأ ما";
            $response['result_object'] = [];
            return response()->json($response);
        }
    }

}
