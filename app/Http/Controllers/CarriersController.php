<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Carriers;
use App\Models\CarriersOrders;
use App\Models\Evaluations;
use App\Models\EvaluationsCustomers;
use App\Models\CarrierTripStations;
use App\Models\customerTourImages;
use App\Models\CarrierTripRepeats;
use App\Models\Brands;
use App\Models\ConstantsTable;
use App\Models\NotifyMe;
use Illuminate\Http\Request;
use JWTAuth;
use DB;

/**
 * Description of CarriersController
 *
 * @author ِAMna
 */
class CarriersController extends Controller {

    function __construct() {
        \Config::set('jwt.user', 'App\Models\Users');
        \Config::set('auth.providers.users.table', 'users');
    }

    public function index(Request $request) {
        $Carriers = Carriers::with(['User', 'Status']);
        if (isset($request->name)) {
            $Carriers = $Carriers->whereHas('User', function($x) use ($request) {
                $x->where('name', 'like', '%' . $request->name . '%');
            });
        }
        if (isset($request->city)) {
            $Carriers = $Carriers->where(function($x) use ($request) {
                $x->where('from_place', 'like', '%' . $request->city . '%');
                $x->orWhere('to_place', 'like', '%' . $request->city . '%');
            });
        }
        if (isset($request->status_id)) {
            $Carriers = $Carriers->where('status_id', $request->status_id);
        }
        return response()->json($Carriers->paginate(20));
    }

    public function search(Request $request) {
        $data = json_decode($request->getContent(), true);
        //dd($data);
        $Carriers = Carriers::with(['User', 'Status', 'waypoints']);

        if (isset($data['transfer_way_id'])) {
            $Carriers = $Carriers->where('transfer_way_id', $data['transfer_way_id']);
        }
        if (isset($data['car_brand_id'])) {
            $Carriers = $Carriers->where('car_brand_id', $data['car_brand_id']);
        }
        if (isset($data['people_count'])) {
            $Carriers = $Carriers->where('person_count', $data['people_count']);
        }
        if (isset($data['men_count'])) {
            $Carriers = $Carriers->where('men_count', $data['men_count']);
        }
        if (isset($data['women_count'])) {
            $Carriers = $Carriers->where('women_count', $data['women_count']);
        }
        if (isset($data['children_count'])) {
            $Carriers = $Carriers->where('children_count', $data['children_count']);
        }
        if (isset($data['babies_count'])) {
            $Carriers = $Carriers->where('babies_count', $data['babies_count']);
        }
        if (isset($data['search_type'])) {
            if ($data['search_type'] == 1 || $data['search_type'] == 4) {
                $Carriers = $Carriers->where('person_count', '>', 0);
                $Carriers = $Carriers->where('things_count', '>', 0);
            } elseif ($data['search_type'] == 2) {
                $Carriers = $Carriers->where('things_count', '>', 0);
            } elseif ($data['search_type'] == 3) {
                $Carriers = $Carriers->where('person_count', '>', 0);
            } else {
                $response['code'] = -1;
                $response['result_num'] = 0;
                $response['result_msg'] = 'لا يوجد نتائج';
                $response['result_object'] = [];
                return response()->json($response);
            }
        }
        if (isset($data['from_date'])) {
//            $Carriers = $Carriers->where(DB::raw('DATE_SUB(DATE_SUB(from_date, INTERVAL flexible_days_before DAY), INTERVAL flexible_minutes_before MINUTE)')
//                    , '<=', $data['from_date']);
//            $Carriers = $Carriers->Where(DB::raw('DATE_ADD(DATE_ADD(from_date, INTERVAL flexible_days_after DAY), INTERVAL flexible_minutes_after MINUTE)')
//                    , '>=', $data['from_date']);
//            
            $Carriers = $Carriers->where(DB::raw('DATE_SUB(DATE_SUB(DATE_ADD(DATE_ADD(from_date, INTERVAL flexible_days_after DAY), INTERVAL flexible_minutes_after MINUTE), INTERVAL flexible_days_before DAY), INTERVAL flexible_minutes_before MINUTE)')
                    , '>=', $data['from_date']);
        }
        if (isset($data['to_date'])) {
            $Carriers = $Carriers->where('to_date', '>=', $data['to_date']);
        }

        if (isset($data['orderBy'])) {
            if ($data['orderBy'] == "points") {
                $Carriers = $Carriers->orderBy('distance', 'desc');
            } elseif ($data['orderBy'] == "distance") {
                $Carriers = $Carriers->orderBy('distance', 'desc');
            } elseif ($data['orderBy'] == "evaluation") {
                $Carriers = $Carriers->orderBy('distance', 'desc');
            } elseif ($data['orderBy'] == "price") {
                $Carriers = $Carriers->orderBy('price', 'asc');
            }
            //$Carriers = $Carriers->where('to_date', "<=", $data['to_date']);
        }


        $result = $Carriers->where('status_id', '!=', 7)->where('status_id', '!=', 8)->paginate($request['per_page']);
        $datadata = $result->toArray();


        if (isset($data['from_latitude']) && isset($data['from_longitude'])) {
            foreach ($datadata['data'] as $key => $res) {
                $mainDistance = Carriers::distance($data['from_latitude'], $data['from_longitude'], $res['from_point_x'], $res['to_point_x'], "K");

                $distancess = [];
                if ($res['waypoints']) {
                    foreach ($res['waypoints'] as $point) {
                        $distance = Carriers::distance($data['from_latitude'], $data['from_longitude'], $point['latitude'], $point['longitude'], "K");
                        array_push($distancess, $distance);
                    }
                }
//                dd($mainDistance);
                if(count($distancess) > 0){
                if (min($distancess) > 1 && $mainDistance > 1) {
                    unset($datadata['data'][$key]);
                }
                } else {
                if ($mainDistance > 1) {
                    unset($datadata['data'][$key]);
                }    
                }
            }
        }

        if (isset($data['to_latitude']) && isset($data['to_longitude'])) {
            foreach ($datadata['data'] as $key => $res) {
                $mainDistance2 = Carriers::distance($data['to_latitude'], $data['to_longitude'], $res['from_point_y'], $res['to_point_y'], "K");
                $distancess2 = [];
                if ($res['waypoints']) {
                    foreach ($res['waypoints'] as $point) {
                        $distance2 = Carriers::distance($data['to_latitude'], $data['to_longitude'], $point['latitude'], $point['longitude'], "K");
                        array_push($distancess2, $distance2);
                    }
                }

//                if ((count($distancess2) > 0 && min($distancess2) > 1) && $mainDistance2 > 1) {
//                    unset($datadata['data'][$key]);
//                }
                if(count($distancess2) > 0){
                if (min($distancess2) > 1 && $mainDistance2 > 1) {
                    unset($datadata['data'][$key]);
                }
                } else {
                if ($mainDistance2 > 1) {
                    unset($datadata['data'][$key]);
                }    
                }
            }
        }

        $datadata['data'] = array_values($datadata['data']);

        foreach ($datadata['data'] as $key => $res) {
            $datadata['data'][$key]['track'] = json_decode($res['track'], true);
            $datadata['data'][$key]['status_id'] = (int) $res['status_id'];
            $datadata['data'][$key]['distance'] = (int) $res['distance'];
            unset($datadata['data'][$key]['user']['password']);
            if ($datadata['data'][$key]['user']['image'] != null) {
                $storage = storage_path(ltrim($datadata['data'][$key]['user']['image'], '/'));
                $storage = explode('public_html', $storage);
                $datadata['data'][$key]['user']['image'] = 'http://' . $request->getHttpHost() . $storage[1];
            }

            if ($datadata['data'][$key]['user'] != null) {
                $user = $datadata['data'][$key]['user'];
                $carriersPoints = Evaluations::where('user_id', $user['id'])->sum('evaluation');
                $carriersCount = Evaluations::where('user_id', $user['id'])->count();
                if ($carriersCount == 0)
                    $carriersAvg = 0;
                else
                    $carriersAvg = $carriersPoints / $carriersCount;

                $customersPoints = EvaluationsCustomers::where('user_id', $user['id'])->sum('evaluation');
                $customersCount = EvaluationsCustomers::where('user_id', $user['id'])->count();
                if ($customersCount == 0)
                    $customersAvg = 0;
                else
                    $customersAvg = $customersPoints / $customersCount;

                $datadata['data'][$key]['user']['rating_average'] = $carriersAvg + $customersAvg;
                $datadata['data'][$key]['user']['rating_count'] = $carriersCount + $customersCount;
            }
        }

        $count = count($datadata['data']);
        if ($count > 0) {
            $response['code'] = 1;
            $response['result_num'] = $count;
            $response['result_msg'] = 'تم العثور على البيانات المطلوبة';
            $response['result_object'] = [$datadata];
        } else {
            $response['code'] = -1;
            $response['result_num'] = $count;
            $response['result_msg'] = 'لا يوجد نتائج';
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function store(Request $request) {
        //dd(1);
        $data = $request->toArray();
        //dd($data);
        DB::beginTransaction();
        $user = JWTAuth::parseToken()->authenticate();
        //dd($user);
        $data['user_id'] = $user->id;
        $data['from_point_x'] = $data['from_latitude'];
        $data['to_point_x'] = $data['from_longitude'];
        $data['from_point_y'] = $data['to_latitude'];
        $data['to_point_y'] = $data['to_longitude'];
        $data['date'] = date('Y-m-d');
        $data['status_id'] = 4;
        $data['person_count'] = $data['person_count'];

        if ((!isset($data['from_latitude']) && $data['from_latitude'] == '') ||
                (!isset($data['from_longitude']) && $data['from_longitude'] == '') ||
                (!isset($data['to_latitude']) && $data['to_latitude'] == '') ||
                (!isset($data['to_longitude']) && $data['to_longitude'] == '') ||
                (!isset($data['from_date']) && $data['from_date'] == '') ||
                (!isset($data['to_date']) && $data['to_date'] == '') ) {
            $response["code"] = -2;
            $response['result_num'] = 0;
            $response["result_msg"] = 'يجب ادخال جميع الحقول المطلوبة';
            $response['result_object'] = [];
            return response()->json($response);
        }

        if ($data['person_count'] < ($data['men_count'] + $data['women_count'] + $data['children_count'] + $data['babies_count'])) {
            $response["code"] = -5;
            $response['result_num'] = 0;
            $response["result_msg"] = 'يجب أن يكون عدد الأشخاص أكبر أو يساوي مجموع التفاصيل';
            $response['result_object'] = [];
            return response()->json($response);
        }

        $carrierTour = Carriers::orderBy('order_id', 'desc')->first();
        if ($carrierTour)
            $data['order_id'] = $carrierTour->order_id + 1;
        else
            $data['order_id'] = 1;

        $distance = Carriers::distance($data['from_latitude'], $data['from_longitude'], $data['to_latitude'], $data['to_longitude'], "K");
        $data['distance'] = round($distance, 2);

        if ($data['from_date'] < date('Y-m-d')) {
            $response["code"] = -3;
            $response['result_num'] = 0;
            $response["result_msg"] = 'يجب أن يكون تاريخ بداية الرحلة أكبر او يساوي تاريخ اليوم';
            $response['result_object'] = [];
            return response()->json($response);
        }

        if ($data['to_date'] < $data['from_date']) {
            $response["code"] = -4;
            $response['result_num'] = 0;
            $response["result_msg"] = 'يجب أن يكون تاريخ نهاية الرحلة أكبر او يساوي تاريخ بداية الرحلة';
            $response['result_object'] = [];
            return response()->json($response);
        }

        $track = json_decode($data['track'], true);
        if ($track == null) {
            $response["code"] = -6;
            $response['result_num'] = 0;
            $response["result_msg"] = 'صيغة المسار غير صحيحة، يجب كتابتها بشكل صحيح';
            $response['result_object'] = [];
            return response()->json($response);
        }
        $tour = Carriers::create($data);

        if (isset($data['Stations'])) {
            foreach ($data['Stations'] as $station) {
                $station['carrier_trip_id'] = $tour->id;
                CarrierTripStations::create($station);
            }
        }


        if (isset($data['place_images'])) {
            foreach ($_FILES['place_images']['name'] as $key => $file) {
                //dd($_FILES['things_images']['tmp_name'][$key]);
                $extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                $tmp = '/images/'
                        . '_'
                        . round(microtime(true) * 1000)
                        . '.'
                        . $extension;
                //$base2image->save(base_path() . '/storage'.$tmp);
                if (move_uploaded_file($_FILES['place_images']['tmp_name'][$key], base_path() . '/storage' . $tmp)) {
                    $image['tour_id'] = $tour->id;
                    $image['type'] = 3; //صور المكان
                    $image['image'] = $tmp;

                    customerTourImages::create($image);
                }
            }
        }

        if ($data['want_repeat'] == 1) {
            $repeats = $data['repeats'];
            $repeats['carrier_trip_id'] = $tour->id;
            if ($repeats['end_date'] <= date('Y-m-d')) {
                $response["code"] = -7;
                $response['result_num'] = 0;
                $response["result_msg"] = 'يجب أن يكون تاريخ نهاية التكرار أكبر من تاريخ اليوم';
                $response['result_object'] = [];
                return response()->json($response);
            }
            if ($repeats['end_date'] <= $data['from_date']) {
                $response["code"] = -8;
                $response['result_num'] = 0;
                $response["result_msg"] = 'يجب أن يكون تاريخ نهاية التكرار أكبر من تاريخ بداية الرحلة';
                $response['result_object'] = [];
                return response()->json($response);
            }
            CarrierTripRepeats::create($repeats);
        }
        // }

        $tour->waypoints = CarrierTripStations::where('carrier_trip_id', $tour->id)->get();
        $tour->track = json_decode($tour->track, true);
        $tour->status_id = (int) $tour->status_id;
        $tour->distance = (int) $tour->distance;

        DB::commit();

        if ($tour) {
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = 'تم الإضافة بنجاح';
            $response['result_object'] = [$tour];
        } else {
            $response["code"] = -1;
            $response['result_num'] = 0;
            $response["result_msg"] = 'حدث خطأ ما ولم يتم الحفظ';
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function update($id, Request $request) {
        $data = $request->toArray();
        DB::beginTransaction();
        $user = JWTAuth::parseToken()->authenticate();
        $trip = Carriers::with(['User', 'Status'])->find($id);
        if ($trip) {
            if ($trip->status_id != 4) {
                $response['code'] = -1;
                $response['result_num'] = 0;
                $response['result_msg'] = "لا يمكنك تعديل رحلة حالتها ليست جديدة";
                $response['result_object'] = [];
                return response()->json($response);
            }
            $orders = CarriersOrders::where('carrier_id', $trip->id)->count();
            if ($orders > 0) {
                $response['code'] = -2;
                $response['result_num'] = 0;
                $response['result_msg'] = "لا يمكنك تعديل رحلة تم عمل طلب مشاركة عليها";
                $response['result_object'] = [];
                return response()->json($response);
            }
            $data['from_point_x'] = $data['from_latitude'];
            $data['to_point_x'] = $data['from_longitude'];
            $data['from_point_y'] = $data['to_latitude'];
            $data['to_point_y'] = $data['to_longitude'];
            $data['person_count'] = $data['person_count'];
            if ($user->id != $trip->user_id) {
                $response['code'] = -3;
                $response['result_num'] = 0;
                $response['result_msg'] = "لا يمكنك تعديل رحلة لم تقم بإضافتها";
                $response['result_object'] = [];
                return response()->json($response);
            }
            if ((!isset($data['from_latitude']) && $data['from_latitude'] == '') ||
                    (!isset($data['from_longitude']) && $data['from_longitude'] == '') ||
                    (!isset($data['to_latitude']) && $data['to_latitude'] == '') ||
                    (!isset($data['to_longitude']) && $data['to_longitude'] == '') ||
                    (!isset($data['from_date']) && $data['from_date'] == '') ||
                    (!isset($data['to_date']) && $data['to_date'] == '') ||
                    (!isset($data['transfer_way_id']) && $data['transfer_way_id'] == '')) {
                $response["code"] = -4;
                $response['result_num'] = 0;
                $response["result_msg"] = 'يجب ادخال جميع الحقول المطلوبة';
                $response['result_object'] = [];
                return response()->json($response);
            }

            if ($data['person_count'] < ($data['men_count'] + $data['women_count'] + $data['children_count'] + $data['babies_count'])) {
                $response["code"] = -5;
                $response['result_num'] = 0;
                $response["result_msg"] = 'يجب أن يكون عدد الأشخاص أكبر أو يساوي مجموع التفاصيل';
                $response['result_object'] = [];
                return response()->json($response);
            }
            $distance = Carriers::distance($data['from_latitude'], $data['from_longitude'], $data['to_latitude'], $data['to_longitude'], "K");
            $data['distance'] = round($distance, 2);

//        if ($data['from_date'] < date('Y-m-d')) {
//            $response["code"] = -6;
//            $response['result_num'] = 0;
//            $response["result_msg"] = 'يجب أن يكون تاريخ بداية الرحلة أكبر او يساوي تاريخ اليوم';
//            $response['result_object'] = '';
//            return response()->json($response);
//        }

            if ($data['to_date'] < $data['from_date']) {
                $response["code"] = -6;
                $response['result_num'] = 0;
                $response["result_msg"] = 'يجب أن يكون تاريخ نهاية الرحلة أكبر او يساوي تاريخ بداية الرحلة';
                $response['result_object'] = [];
                return response()->json($response);
            }

            $track = json_decode($data['track'], true);
            if ($track == null) {
                $response["code"] = -7;
                $response['result_num'] = 0;
                $response["result_msg"] = 'صيغة المسار غير صحيحة، يجب كتابتها بشكل صحيح';
                $response['result_object'] = [];
                return response()->json($response);
            }

            $trip->update($data);

            if (isset($data['Stations'])) {
                CarrierTripStations::where('carrier_trip_id', $trip->id)->delete();
                foreach ($data['Stations'] as $station) {
                    $station['carrier_trip_id'] = $trip->id;
                    CarrierTripStations::create($station);
                }
            }

            if (isset($data['place_images'])) {
                customerTourImages::where('tour_id', $trip->id)->where('type', 3)->delete();
                foreach ($_FILES['place_images']['name'] as $key => $file) {
                    //dd($_FILES['things_images']['tmp_name'][$key]);
                    $extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                    $tmp = '/images/'
                            . '_'
                            . round(microtime(true) * 1000)
                            . '.'
                            . $extension;
                    //$base2image->save(base_path() . '/storage'.$tmp);
                    if (move_uploaded_file($_FILES['place_images']['tmp_name'][$key], base_path() . '/storage' . $tmp)) {
                        $image['tour_id'] = $trip->id;
                        $image['type'] = 3; //صور المكان
                        $image['image'] = $tmp;

                        customerTourImages::create($image);
                    }
                }
            }

            if ($data['want_repeat'] == 1) {
                CarrierTripRepeats::where('carrier_trip_id', $trip->id)->delete();
                $repeats = $data['repeats'];
                $repeats['carrier_trip_id'] = $trip->id;
                if ($repeats['end_date'] <= date('Y-m-d')) {
                    $response["code"] = -8;
                    $response['result_num'] = 0;
                    $response["result_msg"] = 'يجب أن يكون تاريخ نهاية التكرار أكبر من تاريخ اليوم';
                    $response['result_object'] = [];
                    return response()->json($response);
                }
                if ($repeats['end_date'] <= $data['from_date']) {
                    $response["code"] = -9;
                    $response['result_num'] = 0;
                    $response["result_msg"] = 'يجب أن يكون تاريخ نهاية التكرار أكبر من تاريخ بداية الرحلة';
                    $response['result_object'] = [];
                    return response()->json($response);
                }
                CarrierTripRepeats::create($repeats);
            }

            $trip->waypoints = CarrierTripStations::where('carrier_trip_id', $trip->id)->get();
            $trip->track = json_decode($trip->track, true);
            $trip->status_id = (int) $trip->status_id;
            $trip->distance = (int) $trip->distance;
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = 'تم التعديل بنجاح';
            $response['result_object'] = [$trip];
        } else {
            $response['code'] = -10;
            $response['result_num'] = 0;
            $response['result_msg'] = "الرحلة غير موجودة";
            $response['result_object'] = [];
        }
        DB::commit();


        return response()->json($response);
    }

    public function show($id) {
        $response['carriers'] = Carriers::with(['User', 'Status'])->find($id);
        $response['orders'] = CarriersOrders::with('User')->where('carrier_id', $id)->get();
        $response['evaluations'] = Evaluations::with('User')->where('carrier_id', $id)->get();
        return response()->json($response);
    }

    public function getTrip($id, Request $request) {
        $trip = Carriers::with(['User', 'Status', 'waypoints'])->find($id);

        if ($trip) {
            unset($trip->User->password);
            $trip->order_id = str_pad($trip->order_id, 9, '0', STR_PAD_LEFT);
            $trip->track = json_decode($trip->track, true);
            $brands = Brands::where('BrandId', $trip->car_brand_id)->first();
            $trip->car_brand_name = $brands->Name;
            $transferWays = ConstantsTable::find($trip->transfer_way_id);
            $trip->transfer_way_name = $transferWays->name;

            $images = customerTourImages::where('tour_id', $id)->where('type', 3)->get();
//            dd($images);
            if ($images) {
                foreach ($images as $image) {
                    $storage = storage_path(ltrim($image->image, '/'));
                    $storage = explode('public_html', $storage);
                    $image->image = 'http://' . $request->getHttpHost() . $storage[1];
                }

                $trip->place_images = $images;
            } else {
                $trip->place_images = '';
            }

            if (isset($trip->User->image) && ($trip->User->image != null || $trip->User->image != '')) {
                $storage = storage_path(ltrim($trip->User->image, '/'));
                $storage = explode('public_html', $storage);
                $trip->User->image = 'http://' . $request->getHttpHost() . $storage[1];
            }

            $trip->status_id = (int) $trip->status_id;
            $trip->distance = (int) $trip->distance;

            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = "تم العثور على البيانات المطلوبة";
            $response['result_object'] = [$trip];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }

        return response()->json($response);
    }

    public function shareTrip(Request $request) {
        $data = json_decode($request->getContent(), true);
        //dd($data);
//        if ($data['price'] == "") {
//            $response["code"] = -2;
//            $response['result_num'] = 0;
//            $response["result_msg"] = 'السعر مطلوب';
//            $response['result_object'] = [];
//            return response()->json($response);
//        }
        $trip = Carriers::find($data['carrier_id']);
        if (!$trip) {
            $response["code"] = -3;
            $response['result_num'] = 0;
            $response["result_msg"] = 'الرحلة غير موجودة، يرجى التأكد من البيانات المدخلة';
            $response['result_object'] = [];
            return response()->json($response);
        } else {
            if ($data['seats_count'] > $trip->person_count) {
                $response["code"] = -4;
                $response['result_num'] = 0;
                $response["result_msg"] = 'عدد المشاركين أكبر من العدد المسموح به بالرحلة';
                $response['result_object'] = [];
                return response()->json($response);
            }

            $seatCounts = CarriersOrders::where('carrier_id', $data['carrier_id'])->where('status_id', 2)->sum('seats_count');
            if (($seatCounts + $data['seats_count']) > $trip->person_count) {
                $response["code"] = -6;
                $response['result_num'] = 0;
                $response["result_msg"] = 'عدد المقاعد المتوفر أقل من عدد المشاركين';
                $response['result_object'] = [];
                return response()->json($response);
            }

            $data['status_id'] = 1;
            $user = JWTAuth::parseToken()->authenticate();
            $data['user_id'] = $user->id;
            $userOrder = CarriersOrders::where('user_id', $user->id)->where('carrier_id', $data['carrier_id'])
                            ->where('status_id', '!=', 3)->where('status_id', '!=', 4)->first();
            if ($userOrder) {
                $response["code"] = -5;
                $response['result_num'] = 0;
                $response["result_msg"] = 'يوجد طلب سابق لنفس الرحلة !';
                $response['result_object'] = [];
                return response()->json($response);
            }

            if ($user->id == $trip->user_id) {
                $response["code"] = -7;
                $response['result_num'] = 0;
                $response["result_msg"] = 'لايمكنك المشاركة برحتلك الخاصة';
                $response['result_object'] = [];
                return response()->json($response);
            }
            $distance = Carriers::distance($trip->from_point_x, $trip->to_point_x, $data['latitude'], $data['longitude'], "K");
            $data['distance'] = round($distance, 2);
            $carrierOrder = CarriersOrders::create($data);
            if ($carrierOrder) {
                $carrierOrder->status_id = (int) $carrierOrder->status_id;
                $response["code"] = 1;
                $response['result_num'] = 1;
                $response["result_msg"] = 'تم مشاركتك بالرحلة بنجاح';
                $response['result_object'] = [$carrierOrder];
            } else {
                $response["code"] = -1;
                $response['result_num'] = 0;
                $response["result_msg"] = 'فشلت مشاركتك بالرحلة ، حدث مشكلة يرجى المحاولة مرة أخرى';
                $response['result_object'] = [];
            }
        }
        return response()->json($response);
    }

    public function table(Request $request) {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $id = $userAuth->id;

        $myTable = Carriers::with('waypoints');
        if (isset($request['date'])) {
            $myTable = $myTable->whereDate('from_date', $request['date']);
        }
        if (isset($request['status_id'])) {
            $myTable = $myTable->where('status_id', $request['status_id']);
        }

        $myTable = $myTable->where('user_id', $id)
                ->orWhere(function($query) use ($id, $request) {
            if (isset($request['date'])) {
                $query->whereDate('from_date', '=', $request['date']);
            }
            if (isset($request['status_id'])) {
                $query->where('status_id', $request['status_id']);
            }
            $query->WhereHas('carrierOrders', function( $q ) use ($id) {
                $q->where('user_id', $id)->where('status_id', '!=', 4);
            });
        });
        //}
        $myTable = $myTable->orderBy('id', 'desc')->paginate($request['per_page']);
        $count = count($myTable);
        if ($count > 0) {
            foreach ($myTable as $trip) {
                $trip->track = json_decode($trip->track, true);
                $trip->status_id = (int) $trip->status_id;
                $trip->distance = (int) $trip->distance;
            }
            $response["code"] = 1;
            $response['result_num'] = $count;
            $response["result_msg"] = "تم العثور على البيانات المطلوبة";
            $response['result_object'] = [$myTable];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }

        return response()->json($response);
    }

    public function confirmOrCancel(Request $request) {
        $data = json_decode($request->getContent(), true);
        $trip = Carriers::find($data['trip_id']);
        $userAuth = JWTAuth::parseToken()->authenticate();
        $uid = $userAuth->id;
        if ($trip) {
            if ($uid != $trip->user_id) {
                $response['code'] = -2;
                $response['result_num'] = 0;
                $response['result_msg'] = "غير مسموح لك بقبول أو رفض الطلب";
                $response['result_object'] = [];
                return response()->json($response);
            }
            $trip->status_id = $data['status_id'];
            $trip->cancel_reason = $data['cancel_reason'];
            $trip->save();
            $trip->status_id = (int) $trip->status_id;
            $trip->distance = (int) $trip->distance;
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = "تم الحفظ بنجاح";
            $response['result_object'] = [$trip];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function endTrip(Request $request) {
        $data = json_decode($request->getContent(), true);
        $trip = Carriers::find($data['trip_id']);
        if ($trip) {
            $trip->status_id = $data['status_id'];
            $trip->save();
            $trip->status_id = (int) $trip->status_id;
            $trip->distance = (int) $trip->distance;
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = "تم إنهاء الرحلة بنجاح";
            $response['result_object'] = [$trip];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function candidates($id, Request $request) {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $userid = $userAuth->id;
        $candidates = CarriersOrders::with('User')->where('carrier_id', $id)->where('status_id', '!=', 4)
                        ->where(function($query) use ($userid) {
                            $query->whereHas('Trip', function($q) use ($userid) {
                                $q->where('user_id', $userid);
                            })
                            ->orWhere('user_id', $userid);
                        })->get();
        //dd($candidates);
        $candidatesCount = CarriersOrders::with('User')->where('carrier_id', $id)->where('status_id', '!=', 4)
                        ->where(function($query) use ($userid) {
                            $query->whereHas('Trip', function($q) use ($userid) {
                                $q->where('user_id', $userid);
                            })
                            ->orWhere('user_id', $userid);
                        })->count();
        if ($candidatesCount > 0) {
            foreach ($candidates as $cand) {
                $carriersPoints = Evaluations::where('user_id', $cand->user_id)->sum('evaluation');
                $carriersCount = Evaluations::where('user_id', $cand->user_id)->count();
                if ($carriersCount == 0)
                    $carriersAvg = 0;
                else
                    $carriersAvg = $carriersPoints / $carriersCount;

                $customersPoints = EvaluationsCustomers::where('user_id', $cand->user_id)->sum('evaluation');
                $customersCount = EvaluationsCustomers::where('user_id', $cand->user_id)->count();
                if ($customersCount == 0)
                    $customersAvg = 0;
                else
                    $customersAvg = $customersPoints / $customersCount;

                $cand->rating_average = $carriersAvg + $customersAvg;
                $cand->rating_count = $carriersCount + $customersCount;

                unset($cand->user->password);
                if ($cand->user->image != null) {
                    $storage = storage_path(ltrim($cand->user->image, '/'));
                    $storage = explode('public_html', $storage);
                    $cand->user->image = 'http://' . $request->getHttpHost() . $storage[1];
                }

                $cand->status_id = (int) $cand->status_id;
            }
            $response["code"] = 1;
            $response['result_num'] = $candidatesCount;
            $response["result_msg"] = "تم العثور على النتائج المطلوبة";
            $response['result_object'] = $candidates;
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function viewCandidates($id, Request $request) {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $userid = $userAuth->id;
        $candidates = CarriersOrders::with('User', 'evaluates')
                        ->where(function($query) use ($userid) {
                            $query->whereHas('Trip', function($q) use ($userid) {
                                $q->where('user_id', $userid);
                            })->orWhere('user_id', $userid);
                        })->find($id);
        if ($candidates) {
            $carriersPoints = Evaluations::where('user_id', $candidates->user_id)->sum('evaluation');
            $carriersCount = Evaluations::where('user_id', $candidates->user_id)->count();
            if ($carriersCount == 0)
                $carriersAvg = 0;
            else
                $carriersAvg = $carriersPoints / $carriersCount;

            $customersPoints = EvaluationsCustomers::where('user_id', $candidates->user_id)->sum('evaluation');
            $customersCount = EvaluationsCustomers::where('user_id', $candidates->user_id)->count();
            if ($customersCount == 0)
                $customersAvg = 0;
            else
                $customersAvg = $customersPoints / $customersCount;

            $candidates->rating_average = $carriersAvg + $customersAvg;
            $candidates->rating_count = $carriersCount + $customersCount;

            unset($candidates->user->password);
            if ($candidates->user->image != null) {
                $storage = storage_path(ltrim($candidates->user->image, '/'));
                $storage = explode('public_html', $storage);
                $candidates->user->image = 'http://' . $request->getHttpHost() . $storage[1];
            }
            $candidates->status_id = (int) $candidates->status_id;
            
//            $evaluate = Evaluations::where('user_id', $candidates->user_id)->where('carrier_id', $candidates->carrier_id)->first();
//            if($evaluate) {
//            $candidates->is_evaluate = 1;   
//            $candidates->evaluate_value = $evaluate->evaluation; 
//            } else {
//            $candidates->is_evaluate = 0;   
//            $candidates->evaluate_value = null;   
//            }

            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = "تم العثور على النتائج المطلوبة";
            $response['result_object'] = [$candidates];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function acceptOrRefuseCandidates($id, Request $request) {
        $candidates = CarriersOrders::with('Trip')->find($id);
        $data = json_decode($request->getContent(), true);

        if ($data['status_id'] != 2 && $data['status_id'] != 3) {
            $response['code'] = -2;
            $response['result_num'] = 0;
            $response['result_msg'] = "الحالة المدخلة غير صحيحة";
            $response['result_object'] = [];
            return response()->json($response);
        }

        $userAuth = JWTAuth::parseToken()->authenticate();
        $uid = $userAuth->id;
        if ($candidates) {
            if ($uid != $candidates->Trip->user_id) {
                $response['code'] = -3;
                $response['result_num'] = 0;
                $response['result_msg'] = "غير مسموح لك بقبول أو رفض الطلب";
                $response['result_object'] = [];
                return response()->json($response);
            }
            $candidates->status_id = $data['status_id'];

            $candidates->save();

            if ($data['status_id'] == 2) {
                $trip = Carriers::find($candidates->Trip->id);
                $seats = CarriersOrders::where('carrier_id', $trip->id)->where('status_id', 2)->sum('seats_count');
                if ($seats == $trip->person_count) {
                    $trip->status_id = 6;
                    $trip->save();
                }
            }

            $candidates->status_id = (int) $candidates->status_id;
            $candidates->Trip->track = json_decode($candidates->Trip->track, true);
            $candidates->Trip->status_id = (int) $candidates->Trip->status_id;
            $candidates->Trip->distance = (int) $candidates->Trip->distance;

            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = "تم الحفظ بنجاح ";
            $response['result_object'] = [$candidates];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function closeExpiredTrips() {
        $endTrips = Carriers::where('to_date', '<', date('Y-m-d'))->get();

        foreach ($endTrips as $trip) {
            $trip->status_id = 7;
            $trip->save();
        }
        if (count($endTrips) > 0) {
            $endTrips->status_id = (int) $endTrips->status_id;
            $endTrips->distance = (int) $endTrips->distance;

            $response["code"] = 1;
            $response['result_num'] = count($endTrips);
            $response["result_msg"] = "تمت العملية بنجاح";
            $response['result_object'] = $endTrips;
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد رحلات منتهية التاريخ";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function candidateCancelRequest($id) {
        $candidates = CarriersOrders::with('Trip')->find($id);
        $userAuth = JWTAuth::parseToken()->authenticate();
        $uid = $userAuth->id;
        if ($candidates) {
            if ($uid != $candidates->user_id) {
                $response['code'] = -2;
                $response['result_num'] = 0;
                $response['result_msg'] = "غير مسموح لك بالغاء طلب ليس لك";
                $response['result_object'] = [];
                return response()->json($response);
            }
//            if ($candidates->status_id != 1) {
//                $response['code'] = -3;
//                $response['result_num'] = 0;
//                $response['result_msg'] = "غير مسموح لك بالغاء طلب بعد قبوله";
//                $response['result_object'] = [];
//                return response()->json($response);
//            }

            $candidates->status_id = 4;
            $candidates->save();
            if($candidates->Trip->status_id == 6){
              $candidates->Trip->status_id = 5;
              $candidates->Trip->save();
            }
            $candidates->status_id = (int) $candidates->status_id;
            $candidates->Trip->track = json_decode($candidates->Trip->track, true);
            $candidates->Trip->status_id = (int) $candidates->Trip->status_id;
            $candidates->Trip->distance = (int) $candidates->Trip->distance;
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = "تم إلغاء مشاركتك بنجاح ";
            $response['result_object'] = [$candidates];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }

        return response()->json($response);
    }

    public function notifyMe(Request $request) {
        $data = json_decode($request->getContent(), true);

        $userAuth = JWTAuth::parseToken()->authenticate();
        $uid = $userAuth->id;
        if (!$uid) {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "عليك تسجيل الدخول أولاً";
            $response['result_object'] = [];
            return response()->json($response);
        }

        $data['type'] = 1;
        $data['user_id'] = $uid;

        $notif = NotifyMe::create($data);
        if ($notif) {
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = "سيتم إرسال إشعار لك";
            $response['result_object'] = [];
            return response()->json($response);
        } else {
            $response['code'] = -2;
            $response['result_num'] = 0;
            $response['result_msg'] = "حدث خطأ ما";
            $response['result_object'] = [];
            return response()->json($response);
        }
    }

    public function evaluate($id, Request $request) {
        $data = json_decode($request->getContent(), TRUE);
        if ($data['points'] == "") {
            $response["code"] = -2;
            $response['result_num'] = 0;
            $response["result_msg"] = 'التقييم مطلوب';
            $response['result_object'] = [];
            return response()->json($response);
        }
        $data1['evaluation'] = $data['points'];
        $data1['carrier_id'] = $data['trip_id'];
        $data1['notes'] = $data['notes'];
        $data1['user_id'] = $id;
        $data1['date'] = date("Y-m-d");
        
        $oldEvaluations = Evaluations::where('carrier_id', $data['trip_id'])
                ->where('user_id', $id)->count();
        if($oldEvaluations > 0){
            $response["code"] = -3;
            $response['result_num'] = 0;
            $response["result_msg"] = 'تم التقييم مسبقاَ';
            $response['result_object'] = []; 
            return response()->json($response);
        }

        $carrierOrder = Evaluations::create($data1);
        if ($carrierOrder) {
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = 'تم التقييم بنجاح';
            $response['result_object'] = [$carrierOrder];
        } else {
            $response["code"] = -1;
            $response['result_num'] = 0;
            $response["result_msg"] = 'حدث مشكلة أثناء عملية التقييم، يرجى المحاولة مرة أخرى';
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

}
