<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Admins
 *
 * @author Amna
 */
class Admins extends Model {
    protected $table = 'admins';
    protected $fillable = ['name', 'username', 'password', 'email', 'mobile'];

//    public function roles() {
//        $roles = [
//            'name' => 'required',
//        ];
//     
//        return $roles;
//    }
}
