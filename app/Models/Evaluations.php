<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of evaluations
 *
 * @author ِAMna
 */
class Evaluations extends Model {
    protected $table = 'evaluations';
    protected $fillable = ['user_id', 'notes', 'evaluation', 'date', 'carrier_id'];
    
    public function User() {
        return $this->belongsTo(\App\Models\Users::class, 'user_id', 'id');
    }

}
