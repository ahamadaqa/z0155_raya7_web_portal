<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of CustomerTourStations
 *
 * @author Amna
 */
class customerTourImages extends Model{
    protected $table = 'customer_tour_images';
    protected $fillable = ['tour_id', 'image', 'type'];

}
