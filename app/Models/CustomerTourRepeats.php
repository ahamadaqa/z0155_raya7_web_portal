<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of CustomerTourRepeats
 *
 * @author Amna
 */
class CustomerTourRepeats extends Model {
    protected $table = 'tour_repeats';
    protected $fillable = ['tour_id', 'every_day', 'every_sat', 'every_sun', 'every_mon', 'every_tue', 'every_wed',
        'every_thr', 'every_fri', 'end_date'];

}
