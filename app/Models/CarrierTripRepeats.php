<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of CustomerTourRepeats
 *
 * @author Amna
 */
class CarrierTripRepeats extends Model {
    protected $table = 'carrier_trip_repeats';
    protected $fillable = ['carrier_trip_id', 'every_day', 'every_sat', 'every_sun', 'every_mon', 'every_tue', 'every_wed',
        'every_thr', 'every_fri', 'end_date'];

}
