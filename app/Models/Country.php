<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


/**
 * Description of Country
 *
 * @author Hp
 */
class Country extends Model {
    protected $table = 'country';
    protected $fillable = ['Name', 'EnglishName', 'Nationality', 'EnglishNationality', 'name3', 'name4', 'name5'];

}
