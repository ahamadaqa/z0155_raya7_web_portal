<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of NotifyMe
 *
 * @author aymh_
 */
class NotifyMe extends Model {
    protected $table = 'filter_notifications';
    protected $fillable = ['user_id', 'type', 'from_latitude', 'from_longitude', 'to_latitude', 'to_longitude', 'transfer_way_id'
        ,'car_brand_id', 'people_count', 'men_count', 'women_count', 'children_count', 'babies_count', 'from_date', 'to_date', 'search_type'];

}
