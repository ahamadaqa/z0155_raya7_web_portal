<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of CarriersOrders
 *
 * @author Amna
 */
class CarriersOrders extends Model {
    protected $table = 'carriers_orders';
    protected $fillable = ['user_id', 'from_date', 'to_date', 'status_id', 'carrier_id', 'price', 'notes',
        'alone', 'i_have_things', 'seats_count', 'weight', 'size', 'distance'];
    
    public function User() {
        return $this->belongsTo(\App\Models\Users::class, 'user_id', 'id');
    }
    
    public function Trip() {
        return $this->belongsTo(\App\Models\Carriers::class, 'carrier_id', 'id');
    }
    
    public function evaluates(){
         return $this->hasMany(\App\Models\Evaluations::class, 'carrier_id', 'carrier_id');
    }
}
