<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of userImages
 *
 * @author Amna
 */
class userImages extends Model {
    protected $table = 'user_images';
    protected $fillable = ['user_id', 'image'];

}
