<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of CustomersOrders
 *
 * @author Amna
 */
class CustomersOrders extends Model {
    protected $table = 'customers_tours_orders';
    protected $fillable = ['user_id', 'from_date', 'to_date', 'status_id', 'customer_tour_id', 'price', 'notes', 'distance'];
    
    public function User() {
        return $this->belongsTo(\App\Models\Users::class, 'user_id', 'id');
    }
    
    public function TripReq() {
        return $this->belongsTo(\App\Models\Customers::class, 'customer_tour_id', 'id');
    }
    
    public function evaluates(){
         return $this->hasMany(\App\Models\EvaluationsCustomers::class, 'customer_id', 'customer_tour_id');
    }
}
