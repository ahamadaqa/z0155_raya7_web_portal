<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of CustomerTourStations
 *
 * @author Amna
 */
class CarrierTripStations extends Model {
    protected $table = 'carrier_trips_stations';
    protected $fillable = ['latitude', 'longitude', 'carrier_trip_id'];

}
