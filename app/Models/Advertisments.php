<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Advertisments
 *
 * @author Amna
 */
class Advertisments extends Model {
    protected $table = 'advertisments';
    protected $fillable = ['image', 'from_date', 'to_date', 'url', 'mobile'];
}
