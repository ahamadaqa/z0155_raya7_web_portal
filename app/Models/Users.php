<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Users
 *
 * @author Hp
 */
class Users extends Model {
    protected $table = 'users';
    protected $fillable = ['name', 'mobile', 'password', 'email', 'city','status_id','delete_reason','work_id','image'];

    public function carriers() {
        return $this->hasMany(\App\Models\Carriers::class, 'user_id', 'id');
    }
    
    public function customers() {
        return $this->hasMany(\App\Models\Customers::class, 'user_id', 'id');
    }
    
    public function CityName() {
        return $this->belongsTo(\App\Models\City::class, 'city', 'CityId');
    }
    
    public function Country() {
        return $this->belongsTo(\App\Models\Country::class, 'country', 'CountryId');
    }
    
    public function Work() {
        return $this->belongsTo(\App\Models\ConstantsTable::class, 'work_id', 'id');
    }
}
