<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of ConstantsTable
 *
 * @author Amna
 */
class ConstantsTable extends Model {
    protected $table = 'constant_table';
    protected $fillable = ['name', 'type'];
}
