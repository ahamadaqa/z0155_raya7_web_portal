<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Carriers
 *
 * @author Amna
 */
class Carriers extends Model {
    protected $table = 'carriers';
    protected $fillable = ['order_id', 'date', 'status_id', 'transfer_way_id', 'from_date', 'to_date', 'from_place', 'to_place',
        'transfer_type', 'person_count','from_point_x', 'to_point_x', 'from_point_y', 'to_point_y', 'notes', 'user_id','distance', 'cancel_reason',
        'flexible_minutes_before','flexible_minutes_after','flexible_days_before','flexible_days_after','men_count','women_count',
        'children_count','babies_count','person_count','things_count','weight','size','want_repeat', 'dont_want_smoke', 'dont_want_music', 'car_brand_id','price', 'track'];
    
    public function User() {
        return $this->belongsTo(\App\Models\Users::class, 'user_id', 'id');
    }
    
    public function Status() {
        return $this->belongsTo(\App\Models\ConstantsTable::class, 'status_id', 'id');
    }

    public function carrierOrders() {
        return $this->hasMany(\App\Models\CarriersOrders::class, 'carrier_id', 'id');
    }
    
    public function waypoints() {
        return $this->hasMany(\App\Models\CarrierTripStations::class, 'carrier_trip_id', 'id');
    }
    
    public static function distance($lat1, $lon1, $lat2, $lon2, $unit) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }
}
