ALTER TABLE `advertisments` ADD `mobile` VARCHAR(50) NULL AFTER `url`;
ALTER TABLE `users` ADD `image` VARCHAR(250) NULL AFTER `work_id`;
ALTER TABLE `carriers` ADD `distance` FLOAT NOT NULL AFTER `user_id`;
ALTER TABLE `customers_tours` ADD `distance` FLOAT NOT NULL AFTER `user_id`;