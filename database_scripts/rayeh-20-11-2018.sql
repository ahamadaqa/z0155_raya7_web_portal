-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 21, 2018 at 07:13 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rayeh`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `is_super_admin` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `username`, `password`, `mobile`, `email`, `is_super_admin`, `created_at`, `updated_at`) VALUES
(1, 'مدير النظام', 'admin', '123123', NULL, NULL, 1, '2018-10-30 02:14:09', '0000-00-00 00:00:00'),
(2, 'أحمد1', 'أحمد', '123', '(059) 654-1155', 'admin@admin.com', 0, '2018-10-20 19:17:53', '2018-10-20 16:17:53'),
(3, 'تجربة', 'test', '123', '(059) 654-1155', NULL, 0, '2018-10-20 16:20:15', '2018-10-20 16:20:15'),
(4, 'مدير النظام', 'admin@admin.com', '$2y$10$w8g/04MnqvHIHxVjciEa6.U9jIltC80.PhLcaZize5XciLmFGw1wW', NULL, NULL, 0, '2018-11-20 15:36:08', '2018-11-20 13:36:08'),
(5, 'nepras', 'admin@admin', '123', NULL, NULL, 0, '2018-11-10 06:12:42', '2018-11-10 06:12:42');

-- --------------------------------------------------------

--
-- Table structure for table `advertisments`
--

CREATE TABLE `advertisments` (
  `id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `advertisments`
--

INSERT INTO `advertisments` (`id`, `image`, `from_date`, `to_date`, `url`, `created_at`, `updated_at`) VALUES
(2, '/images/_1541840594722.jpg', '2018-11-02', '2018-11-16', 'www', '2018-11-10 07:03:14', '2018-11-10 07:03:14'),
(3, '/images/_1542707260219.jpg', '2018-11-17', '2018-11-30', NULL, '2018-11-20 12:17:07', '2018-11-20 10:17:07');

-- --------------------------------------------------------

--
-- Table structure for table `carriers`
--

CREATE TABLE `carriers` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status_id` int(11) NOT NULL,
  `transfer_way_id` int(11) NOT NULL,
  `from_date` datetime NOT NULL,
  `to_date` datetime NOT NULL,
  `from_place` varchar(50) NOT NULL,
  `to_place` varchar(50) NOT NULL,
  `transfer_type` tinyint(1) NOT NULL,
  `people_count` int(5) DEFAULT NULL,
  `from_point_x` float NOT NULL,
  `to_point_x` float NOT NULL,
  `from_point_y` float NOT NULL,
  `to_point_y` float NOT NULL,
  `notes` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `carriers`
--

INSERT INTO `carriers` (`id`, `order_id`, `date`, `status_id`, `transfer_way_id`, `from_date`, `to_date`, `from_place`, `to_place`, `transfer_type`, `people_count`, `from_point_x`, `to_point_x`, `from_point_y`, `to_point_y`, `notes`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-10-23', 7, 2, '2018-10-26 00:00:00', '2018-10-31 00:00:00', 'جدة', 'مكة', 12, 5, 0, 0, 0, 0, '', 1, '2018-11-21 06:12:59', '0000-00-00 00:00:00'),
(2, 2, '2018-10-16', 8, 2, '2018-10-31 00:00:00', '2018-11-30 00:00:00', 'دبي', 'لوس انجلوس', 13, 0, 0, 0, 0, 0, '', 2, '2018-11-21 06:13:03', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `carriers_orders`
--

CREATE TABLE `carriers_orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `status_id` tinyint(1) NOT NULL,
  `carrier_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `carriers_orders`
--

INSERT INTO `carriers_orders` (`id`, `user_id`, `from_date`, `to_date`, `status_id`, `carrier_id`, `created_at`, `updated_at`) VALUES
(1, 2, '2018-10-26', '2018-10-31', 1, 1, '2018-11-20 07:14:55', '0000-00-00 00:00:00'),
(2, 3, '2018-11-01', '2018-10-27', 2, 1, '2018-11-20 07:14:59', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `constant_table`
--

CREATE TABLE `constant_table` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `constant_table`
--

INSERT INTO `constant_table` (`id`, `name`, `type`) VALUES
(1, 'مفعل', 1),
(2, 'موقوف', 1),
(3, 'تم حذفه', 1),
(4, 'جديد', 2),
(5, 'معلق', 2),
(6, 'مؤكد', 2),
(7, 'منتهى', 2),
(8, 'ملغي', 2),
(9, 'الأقدام', 3),
(10, 'الدراجة الهوائية', 3),
(11, 'الدراجة النارية', 3),
(12, 'السيارة', 3),
(13, 'الطائرة', 3),
(14, 'الباخرة', 3),
(15, 'القطار', 3),
(16, 'الترام', 3),
(17, 'الحافلة', 3);

-- --------------------------------------------------------

--
-- Table structure for table `customers_tours`
--

CREATE TABLE `customers_tours` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status_id` int(11) NOT NULL,
  `transfer_way_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `from_place` varchar(50) NOT NULL,
  `to_place` varchar(50) NOT NULL,
  `transfer_type` tinyint(1) NOT NULL,
  `people_count` int(5) NOT NULL,
  `from_point_x` float NOT NULL,
  `to_point_x` float NOT NULL,
  `from_point_y` float NOT NULL,
  `to_point_y` float NOT NULL,
  `notes` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customers_tours_orders`
--

CREATE TABLE `customers_tours_orders` (
  `id` int(11) NOT NULL,
  `customer_tour_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `status_id` tinyint(1) NOT NULL COMMENT '1- مقبول / 2- مرفوض',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `evaluations`
--

CREATE TABLE `evaluations` (
  `id` int(11) NOT NULL,
  `carrier_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `notes` text NOT NULL,
  `evaluation` int(11) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `evaluations`
--

INSERT INTO `evaluations` (`id`, `carrier_id`, `user_id`, `notes`, `evaluation`, `date`, `created_at`, `updated_at`) VALUES
(1, 2, 2, '', 4, '0000-00-00', '2018-11-20 16:06:16', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `evaluations_customers`
--

CREATE TABLE `evaluations_customers` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `notes` text NOT NULL,
  `evaluation` int(11) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `notify_text` text NOT NULL,
  `admin_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `notify_text`, `admin_id`, `date`, `created_at`, `updated_at`) VALUES
(4, 'jkm', 1, '2018-11-09', '2018-11-09 13:41:18', '2018-11-09 13:41:18'),
(5, 'jj', 1, '2018-11-09', '2018-11-09 13:43:02', '2018-11-09 13:43:02'),
(6, 'jkm', 1, '2018-11-09', '2018-11-09 13:43:07', '2018-11-09 13:43:07'),
(7, 'jkm', 1, '2018-11-09', '2018-11-09 13:45:42', '2018-11-09 13:45:42'),
(8, 'jkm', 1, '2018-11-09', '2018-11-09 13:46:56', '2018-11-09 13:46:56'),
(9, 'jkm', 1, '2018-11-09', '2018-11-09 13:47:05', '2018-11-09 13:47:05'),
(10, 'testtt', 1, '2018-11-09', '2018-11-09 13:48:01', '2018-11-09 13:48:01'),
(11, 'testtt', 1, '2018-11-09', '2018-11-09 13:48:14', '2018-11-09 13:48:14'),
(12, 'jj', 1, '2018-11-09', '2018-11-09 14:03:09', '2018-11-09 14:03:09'),
(13, 'yyy', 1, '2018-11-09', '2018-11-09 14:03:40', '2018-11-09 14:03:40');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `status_id` int(11) NOT NULL,
  `delete_reason` text,
  `work_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `mobile`, `password`, `email`, `city`, `country`, `status_id`, `delete_reason`, `work_id`, `created_at`, `updated_at`) VALUES
(1, 'محمد على', '05995522661', '05995522661', 'info@dd.ps', 'pp', 'pp', 1, '', 0, '2018-10-22 17:20:18', '0000-00-00 00:00:00'),
(2, 'test', '05995522661', '05995522661', 'ff', 'ff', 'ff', 2, NULL, 0, '2018-11-20 17:07:12', '2018-11-20 15:07:12'),
(3, 'محمد على', '05995522661', '05995522661', 'info@dd.ps', 'pp', 'pp', 1, '', 1, '2018-10-22 17:21:03', '0000-00-00 00:00:00'),
(4, 'test', '05995522661', '05995522661', 'ff', 'ff', 'ff', 2, '', 1, '2018-10-22 17:21:03', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advertisments`
--
ALTER TABLE `advertisments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carriers`
--
ALTER TABLE `carriers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carriers$user_id_fk` (`user_id`),
  ADD KEY `carriers$status_id_fk` (`status_id`),
  ADD KEY `carriers$transfer_way_id_fk` (`transfer_way_id`);

--
-- Indexes for table `carriers_orders`
--
ALTER TABLE `carriers_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `carrier_id` (`carrier_id`);

--
-- Indexes for table `constant_table`
--
ALTER TABLE `constant_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers_tours`
--
ALTER TABLE `customers_tours`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customersTour$status_id_fk` (`status_id`),
  ADD KEY `customersTour$transfer_way_id_fk` (`transfer_way_id`),
  ADD KEY `customersTour$user_id_fk` (`user_id`);

--
-- Indexes for table `customers_tours_orders`
--
ALTER TABLE `customers_tours_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customersTourOrders$customer_tour_id_fk` (`customer_tour_id`),
  ADD KEY `customersTourOrders$user_id_fk` (`user_id`);

--
-- Indexes for table `evaluations`
--
ALTER TABLE `evaluations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carrier_id` (`carrier_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `evaluations_customers`
--
ALTER TABLE `evaluations_customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `evaluations$customer_id_fk` (`customer_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications$admin_id_fk` (`admin_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_id_fk` (`status_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `advertisments`
--
ALTER TABLE `advertisments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `carriers`
--
ALTER TABLE `carriers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `carriers_orders`
--
ALTER TABLE `carriers_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `constant_table`
--
ALTER TABLE `constant_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `customers_tours`
--
ALTER TABLE `customers_tours`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers_tours_orders`
--
ALTER TABLE `customers_tours_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `evaluations`
--
ALTER TABLE `evaluations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `evaluations_customers`
--
ALTER TABLE `evaluations_customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `carriers`
--
ALTER TABLE `carriers`
  ADD CONSTRAINT `carriers$status_id_fk` FOREIGN KEY (`status_id`) REFERENCES `constant_table` (`id`),
  ADD CONSTRAINT `carriers$transfer_way_id_fk` FOREIGN KEY (`transfer_way_id`) REFERENCES `constant_table` (`id`),
  ADD CONSTRAINT `carriers$user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `carriers_orders`
--
ALTER TABLE `carriers_orders`
  ADD CONSTRAINT `carriers_orders$carrier_id_fk` FOREIGN KEY (`carrier_id`) REFERENCES `carriers` (`id`),
  ADD CONSTRAINT `carriers_orders$user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `customers_tours`
--
ALTER TABLE `customers_tours`
  ADD CONSTRAINT `customersTour$status_id_fk` FOREIGN KEY (`status_id`) REFERENCES `constant_table` (`id`),
  ADD CONSTRAINT `customersTour$transfer_way_id_fk` FOREIGN KEY (`transfer_way_id`) REFERENCES `constant_table` (`id`),
  ADD CONSTRAINT `customersTour$user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `customers_tours_orders`
--
ALTER TABLE `customers_tours_orders`
  ADD CONSTRAINT `customersTourOrders$customer_tour_id_fk` FOREIGN KEY (`customer_tour_id`) REFERENCES `customers_tours` (`id`),
  ADD CONSTRAINT `customersTourOrders$user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `evaluations`
--
ALTER TABLE `evaluations`
  ADD CONSTRAINT `evaluations$carrier_id_fk` FOREIGN KEY (`carrier_id`) REFERENCES `carriers` (`id`),
  ADD CONSTRAINT `evaluations$user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `evaluations_customers`
--
ALTER TABLE `evaluations_customers`
  ADD CONSTRAINT `evaluations$customer_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `customers_tours` (`id`);

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications$admin_id_fk` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `status_id_fk` FOREIGN KEY (`status_id`) REFERENCES `constant_table` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
