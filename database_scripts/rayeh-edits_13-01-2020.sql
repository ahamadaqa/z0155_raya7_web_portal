ALTER TABLE `carriers_orders` CHANGE `price` `price` INT(11) NULL;
ALTER TABLE `customers_tours_orders` CHANGE `price` `price` INT(11) NULL;
ALTER TABLE `carriers` CHANGE `transfer_way_id` `transfer_way_id` INT(11) NULL, 
CHANGE `car_brand_id` `car_brand_id` INT(11) NULL;
ALTER TABLE `customers_tours` CHANGE `transfer_way_id` `transfer_way_id` INT(11) NULL, 
CHANGE `car_brand_id` `car_brand_id` INT(11) NULL DEFAULT NULL;