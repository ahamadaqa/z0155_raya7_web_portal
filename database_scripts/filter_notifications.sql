-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 17, 2019 at 06:11 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rayeh`
--

-- --------------------------------------------------------

--
-- Table structure for table `filter_notifications`
--

CREATE TABLE `filter_notifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `from_latitude` float DEFAULT NULL,
  `from_longitude` float DEFAULT NULL,
  `to_latitude` float DEFAULT NULL,
  `to_longitude` float DEFAULT NULL,
  `transfer_way_id` int(11) DEFAULT NULL,
  `car_brand_id` int(11) DEFAULT NULL,
  `people_count` int(11) DEFAULT NULL,
  `men_count` int(11) DEFAULT NULL,
  `women_count` int(11) DEFAULT NULL,
  `children_count` int(11) DEFAULT NULL,
  `babies_count` int(11) DEFAULT NULL,
  `from_date` datetime DEFAULT NULL,
  `to_date` datetime DEFAULT NULL,
  `search_type` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `filter_notifications`
--

INSERT INTO `filter_notifications` (`id`, `user_id`, `type`, `from_latitude`, `from_longitude`, `to_latitude`, `to_longitude`, `transfer_way_id`, `car_brand_id`, `people_count`, `men_count`, `women_count`, `children_count`, `babies_count`, `from_date`, `to_date`, `search_type`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 24.4713, 39.4775, 21.435, 39.7065, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-17 00:59:44', '2019-09-17 00:59:44'),
(2, 1, 2, 24.4713, 39.4775, 21.435, 39.7065, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-17 01:00:44', '2019-09-17 01:00:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `filter_notifications`
--
ALTER TABLE `filter_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id_indx` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `filter_notifications`
--
ALTER TABLE `filter_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
