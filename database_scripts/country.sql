-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 31, 2019 at 08:59 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rayeh`
--

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `CountryId` int(11) NOT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `EnglishName` varchar(300) DEFAULT NULL,
  `Nationality` varchar(300) DEFAULT NULL,
  `EnglishNationality` varchar(300) DEFAULT NULL,
  `name3` varchar(300) DEFAULT NULL,
  `name4` varchar(300) DEFAULT NULL,
  `name5` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`CountryId`, `Name`, `EnglishName`, `Nationality`, `EnglishNationality`, `name3`, `name4`, `name5`) VALUES
(1, 'السعودية', 'Saudi Arabia', 'سعودي', NULL, NULL, NULL, NULL),
(2, 'قطر', 'Qatar', 'قطري', NULL, NULL, NULL, NULL),
(3, 'الإمارات العربية المتحدة', 'United Arab Emirates', 'إماراتي', NULL, NULL, NULL, NULL),
(4, 'الكويت', 'Kuwait', 'كويتي', NULL, NULL, NULL, NULL),
(5, 'البحرين', 'Bahrai', 'بحريني', NULL, NULL, NULL, NULL),
(6, 'مصر', 'Egypt', 'مصري', NULL, NULL, NULL, NULL),
(7, 'السودان', 'Suda', 'سوداني', NULL, NULL, NULL, NULL),
(8, 'اليمن', 'Yeme', 'يمني', NULL, NULL, NULL, NULL),
(9, 'العراق', 'Iraq', 'عراقي', NULL, NULL, NULL, NULL),
(10, 'الجزائر', 'Algeria', 'جزائري', NULL, NULL, NULL, NULL),
(11, 'الأردن', 'Jorda', 'أردني', NULL, NULL, NULL, NULL),
(12, 'لبنان', 'Lebano', 'لبناني', NULL, NULL, NULL, NULL),
(13, 'سوريا', 'Syria', 'سوري', NULL, NULL, NULL, NULL),
(14, 'ليبيا', 'Libya', 'ليبيي', NULL, NULL, NULL, NULL),
(15, 'المغرب', 'Morocco', 'مغربي', NULL, NULL, NULL, NULL),
(16, 'موريتانيا', 'Mauritania', 'موريتاني', NULL, NULL, NULL, NULL),
(17, 'عمان', 'Oma', 'عماني', NULL, NULL, NULL, NULL),
(18, 'فلسطين', 'Palestine', 'فلسطيني', NULL, NULL, NULL, NULL),
(19, 'تونس', 'Tunisia', 'تونسي', NULL, NULL, NULL, NULL),
(20, 'الصومال', 'Somalia', 'صومالي', NULL, NULL, NULL, NULL),
(21, 'جيبوتي', 'Djibouti', 'جيبوتي', NULL, NULL, NULL, NULL),
(22, 'جزر القمر', 'Comores', 'جزر القمر', NULL, NULL, NULL, NULL),
(23, 'أندورا', 'Andorra', 'أندوري', NULL, NULL, NULL, NULL),
(24, 'أفغانستان', 'Afghanista', 'أفغانستاني', NULL, NULL, NULL, NULL),
(25, 'أنتيجوا', 'Antigua', 'أنتيجوي', NULL, NULL, NULL, NULL),
(26, 'أنجويلا', 'Anguilla', 'أنجويلي', NULL, NULL, NULL, NULL),
(27, 'ألبانيا', 'Albania', 'ألباني', NULL, NULL, NULL, NULL),
(28, 'أرمينيا', 'Armenia', 'أرميني', NULL, NULL, NULL, NULL),
(29, 'أنجولا', 'Anla', 'أنجولي', NULL, NULL, NULL, NULL),
(30, 'الأرجنتين', 'Argentina', 'أرجنتيني', NULL, NULL, NULL, NULL),
(31, 'ساموا الأمريكية', 'American Samoa', 'ساموي الأمريكي', NULL, NULL, NULL, NULL),
(32, 'النمسا', 'Austria', 'نمسواي', NULL, NULL, NULL, NULL),
(33, 'أستراليا', 'Australia', 'أسترالي', NULL, NULL, NULL, NULL),
(34, 'أروبا', 'Aruba', 'أروبي', NULL, NULL, NULL, NULL),
(35, 'أذربيجان', 'Azerbaija', 'أذربيجاني', NULL, NULL, NULL, NULL),
(36, 'البوسنة والهرسك', 'Bosnia and Herzevina', 'بوسني', NULL, NULL, NULL, NULL),
(37, 'بربادوس', 'Barbados', 'بربادوسي', NULL, NULL, NULL, NULL),
(38, 'بنغلاديش', 'Bangladesh', 'بنغلاديشي', NULL, NULL, NULL, NULL),
(39, 'بلجيكا', 'Belgium', 'بلجيكي', NULL, NULL, NULL, NULL),
(40, 'بوركينا فاسو', 'Burkina Faso', 'بوركيني', NULL, NULL, NULL, NULL),
(41, 'بلغاريا', 'Bulgaria', 'بلغاري', NULL, NULL, NULL, NULL),
(42, 'بوروندي', 'Burundi', 'بوروندي', NULL, NULL, NULL, NULL),
(43, 'بنين', 'Beni', 'بنيني', NULL, NULL, NULL, NULL),
(44, 'سانت بارتيليمي', ' Saint Barthélemy', 'سانت بارتيليمي', NULL, NULL, NULL, NULL),
(45, 'برمودا', 'Bermuda', 'برمودي', NULL, NULL, NULL, NULL),
(46, 'بروناي دار السلام', 'Brunei Darussalam', 'بروناي دار السلام', NULL, NULL, NULL, NULL),
(47, 'بوليفيا', 'Bolivia', 'بوليفي', NULL, NULL, NULL, NULL),
(48, 'البرازيل', 'Brazil', 'برازيلي', NULL, NULL, NULL, NULL),
(49, 'جزر البهاما', 'Bahamas', 'جزر البهاما', NULL, NULL, NULL, NULL),
(50, 'بوتان', 'Bhuta', 'بوتاني', NULL, NULL, NULL, NULL),
(51, 'بوتسوانا', 'Botswana', 'بوتسواني', NULL, NULL, NULL, NULL),
(52, 'روسيا البيضاء', 'Biélorussie', 'روسيا البيضاء', NULL, NULL, NULL, NULL),
(53, 'بليز', 'Belize', 'بليزي', NULL, NULL, NULL, NULL),
(54, 'كندا', 'Canada', 'كندي', NULL, NULL, NULL, NULL),
(55, 'جمهورية الكونغو الديموقراطية', 'Con, Democratic Republic of the (Zaire),', 'كونغولي', NULL, NULL, NULL, NULL),
(56, 'جمهورية أفريقيا الوسطى', 'Central African Republic', 'جمهورية أفريقيا الوسطى', NULL, NULL, NULL, NULL),
(57, 'الكونغو', 'Con', 'كونغولي', NULL, NULL, NULL, NULL),
(58, 'سويسرا', 'Suisse', 'سويسري', NULL, NULL, NULL, NULL),
(59, 'ساحل العاج', 'Ivory Coast', 'ساحل العاج', NULL, NULL, NULL, NULL),
(60, 'جزر كوك', 'Cocos Islands', 'جزر كوك', NULL, NULL, NULL, NULL),
(61, 'تشيلي', 'Chile', 'تشيلي', NULL, NULL, NULL, NULL),
(62, 'الكاميرون', 'Cameroo', 'كاميروني', NULL, NULL, NULL, NULL),
(63, 'الصين', 'China', 'صيني', NULL, NULL, NULL, NULL),
(64, 'كولومبيا', 'Colombia', 'كولومبي', NULL, NULL, NULL, NULL),
(65, 'كوستاريكا', 'Costa Rica', 'كوستاريكي', NULL, NULL, NULL, NULL),
(66, 'كوبا', 'Cuba', 'كوبي', NULL, NULL, NULL, NULL),
(67, 'الرأس الأخضر', 'Cape Verde', 'الرأس الأخضر', NULL, NULL, NULL, NULL),
(68, 'جزيرة الكريسماس', 'Christmas Island', 'جزيرة الكريسماس', NULL, NULL, NULL, NULL),
(69, 'قبرص', 'Cyprus', 'قبرصي', NULL, NULL, NULL, NULL),
(70, 'جمهورية التشيك', 'Czech Republic', 'تشيكي', NULL, NULL, NULL, NULL),
(71, 'ألمانيا', 'Germany', 'ألماني', NULL, NULL, NULL, NULL),
(72, 'الدنمارك', 'Denmark', 'دنماركي', NULL, NULL, NULL, NULL),
(73, 'دومينيكا', 'Dominica', 'دومينيكا', NULL, NULL, NULL, NULL),
(74, 'جمهورية الدومينيكان', 'Dominican Republic', 'دومينيكاني', NULL, NULL, NULL, NULL),
(75, 'الاكوادور', 'Ecuador', 'اكوادوري', NULL, NULL, NULL, NULL),
(76, 'استونيا', 'Estonia', 'استوني', NULL, NULL, NULL, NULL),
(77, 'الصحراء الغربية', 'Western Sahara', 'الصحراء الغربية', NULL, NULL, NULL, NULL),
(78, 'إريتريا', 'Eritrea', 'إريتري', NULL, NULL, NULL, NULL),
(79, 'إسبانيا', 'Spai', 'إسباني', NULL, NULL, NULL, NULL),
(80, 'أثيوبيا', 'Ethiopia', 'أثيوبي', NULL, NULL, NULL, NULL),
(81, 'فنلندا', 'Finland', 'فنلندي', NULL, NULL, NULL, NULL),
(82, 'فيجي', 'Fiji', 'فيجي', NULL, NULL, NULL, NULL),
(83, 'جزر فوكلاند (مالفيناس),', 'Falkland Islands', 'جزر فوكلاند (مالفيناس),', NULL, NULL, NULL, NULL),
(84, 'ميكرونيزيا', 'Micronesia, Federated States of', 'ميكرونيزي', NULL, NULL, NULL, NULL),
(85, 'جزر فارو', 'Faroe Islands', 'جزر فارو', NULL, NULL, NULL, NULL),
(86, 'فرنسا', 'France', 'فرنسي', NULL, NULL, NULL, NULL),
(87, 'الغابون', 'Gabo', 'غابوني', NULL, NULL, NULL, NULL),
(88, 'المملكة المتحدة لبريطانيا العظمى وأيرلندا الشمالية', 'United Kingdom', 'إنجليزي', NULL, NULL, NULL, NULL),
(89, 'غرينادا', 'Grenada', 'غرينادي', NULL, NULL, NULL, NULL),
(90, 'جورجيا', 'Georgia', 'جورجي', NULL, NULL, NULL, NULL),
(91, 'غويانا', 'Guyana', 'غوياني', NULL, NULL, NULL, NULL),
(92, 'غيرنسي', 'Guernsey', 'غيرنسي', NULL, NULL, NULL, NULL),
(93, 'غانا', 'Ghana', 'غاني', NULL, NULL, NULL, NULL),
(94, 'جبل طارق', 'Gibraltar', 'جبل طارق', NULL, NULL, NULL, NULL),
(95, 'جرينلاند', 'Greenland', 'جرينلاندي', NULL, NULL, NULL, NULL),
(96, 'غامبيا', 'Gambia', 'غامبي', NULL, NULL, NULL, NULL),
(97, 'غينيا', 'Guinea', 'غيني', NULL, NULL, NULL, NULL),
(98, 'غوادلوب', 'Guadeloupe', 'غوادلوبي', NULL, NULL, NULL, NULL),
(99, 'غينيا الاستوائية', 'Equatorial Guinea', 'غيني', NULL, NULL, NULL, NULL),
(100, 'اليونان', 'Greece', 'يوناني', NULL, NULL, NULL, NULL),
(101, 'جورجيا الجنوبية وجزر ساندويتش الجنوبية ', 'South Georgia and the South Sandwich Islands', 'جورجيا الجنوبية وجزر ساندويتش الجنوبية ', NULL, NULL, NULL, NULL),
(102, 'غواتيمالا', 'Guatemala', 'غواتيمالي', NULL, NULL, NULL, NULL),
(103, 'غوام', 'Guam', 'غوامي', NULL, NULL, NULL, NULL),
(104, 'غينيا بيساو', 'Guinea-Bissau', 'غيني', NULL, NULL, NULL, NULL),
(105, 'غويانا', 'Guyana', NULL, NULL, NULL, NULL, NULL),
(106, 'هونغ كونغ', 'Hong Kong', 'هونغ كونغ', NULL, NULL, NULL, NULL),
(107, 'كرواتيا', 'Croatia', 'كرواتي', NULL, NULL, NULL, NULL),
(108, 'هايتي', 'Haiti', 'هايتي', NULL, NULL, NULL, NULL),
(109, 'هنغاريا', 'Hungary', 'هنغاري', NULL, NULL, NULL, NULL),
(110, 'أندونيسيا', 'Indonesia', 'أندونيسي', NULL, NULL, NULL, NULL),
(111, 'أيرلندا', 'Ireland', 'أيرلندي', NULL, NULL, NULL, NULL),
(112, 'جزيرة مان', 'Isle of Ma', 'جزيرة مان', NULL, NULL, NULL, NULL),
(113, 'الهند', 'India', 'هندي', NULL, NULL, NULL, NULL),
(114, 'ايران', 'Ira', 'ايراني', NULL, NULL, NULL, NULL),
(115, 'أيسلندا', 'Iceland', 'أيسلندي', NULL, NULL, NULL, NULL),
(116, 'إيطاليا', 'Italy', 'إيطالي', NULL, NULL, NULL, NULL),
(117, 'جيرسي', 'Jersey', 'جيرسي', NULL, NULL, NULL, NULL),
(118, 'جامايكا', 'Jamaica', 'جامايكي', NULL, NULL, NULL, NULL),
(119, 'اليابان', 'Japa', 'ياباني', NULL, NULL, NULL, NULL),
(120, 'كينيا', 'Kenya', 'كيني', NULL, NULL, NULL, NULL),
(121, 'جمهورية قيرغيزستان', 'Kyrgyzsta', 'قيرغيزستاني', NULL, NULL, NULL, NULL),
(122, 'كمبوديا', 'Cambodia', 'كمبودي', NULL, NULL, NULL, NULL),
(123, 'كيريباتي', 'Kiribati', 'كيريباتي', NULL, NULL, NULL, NULL),
(124, 'سانت كيتس ونيفيس', 'Saint Kitts and Nevis', 'سانت كيتس ونيفيس', NULL, NULL, NULL, NULL),
(125, 'كوريا الشمالية', 'North Korea', 'كوري شمالي', NULL, NULL, NULL, NULL),
(126, 'كوريا الجنوبية', 'South Korea', 'كوري جنوبي', NULL, NULL, NULL, NULL),
(127, 'جزر كايمان', 'Cayman Islands', 'جزر كايمان', NULL, NULL, NULL, NULL),
(128, 'كازاخستان', 'Kazakhsta', 'كازاخستاني', NULL, NULL, NULL, NULL),
(129, 'جمهورية لاو الديمقراطية الشعبية', 'Laos', 'جمهورية لاو الديمقراطية الشعبية', NULL, NULL, NULL, NULL),
(130, 'سانت لوسيا', 'Saint Lucia', 'سانت لوسي', NULL, NULL, NULL, NULL),
(131, 'ليختنشتاين', 'Liechtenstei', 'ليختنشتايني', NULL, NULL, NULL, NULL),
(132, 'سريلانكا', 'Sri Lanka', 'سريلانكي', NULL, NULL, NULL, NULL),
(133, 'ليبيريا', 'Liberia', 'ليبيري', NULL, NULL, NULL, NULL),
(134, 'ليسوتو', 'Lesotho', 'ليسوتوي', NULL, NULL, NULL, NULL),
(135, 'ليتوانيا', 'Lithuania', 'ليتواني', NULL, NULL, NULL, NULL),
(136, 'لوكسمبورغ', 'Luxembourg', 'لوكسمبورغي', NULL, NULL, NULL, NULL),
(137, 'لاتفيا', 'Latvia', 'لاتفي', NULL, NULL, NULL, NULL),
(138, 'موناكو', 'Monaco', 'موناكو', NULL, NULL, NULL, NULL),
(139, 'مولدوفا', 'Moldova', 'مولدوفي', NULL, NULL, NULL, NULL),
(140, 'الجبل الأسود', 'Montenegro', 'الجبل الأسود', NULL, NULL, NULL, NULL),
(141, 'مدغشقر', 'Madagascar', 'مدغشقري', NULL, NULL, NULL, NULL),
(142, 'جزر مارشال', 'Marshall Islands', 'مارشالي', NULL, NULL, NULL, NULL),
(143, 'مقدونيا', 'Macedonia', 'مقدوني', NULL, NULL, NULL, NULL),
(144, 'مالي', 'Mali', 'مالي', NULL, NULL, NULL, NULL),
(145, 'ميانمار', 'Myanmar', 'ميانماري', NULL, NULL, NULL, NULL),
(146, 'منغوليا', 'Monlia', 'منغولي', NULL, NULL, NULL, NULL),
(147, 'مارتينيك', 'Martinique', 'مارتينيكي', NULL, NULL, NULL, NULL),
(148, 'مونتسيرات', 'Montserrat', 'مونتسيراتي', NULL, NULL, NULL, NULL),
(149, 'مالطا', 'Malta', 'مالطي', NULL, NULL, NULL, NULL),
(150, 'موريشيوس', 'Mauritius', 'موريشيوسي', NULL, NULL, NULL, NULL),
(151, 'جزر المالديف', 'Maldives', 'مالديفي', NULL, NULL, NULL, NULL),
(152, 'ملاوي', 'Malawi', 'ملاوي', NULL, NULL, NULL, NULL),
(153, 'المكسيك', 'Mexico', 'مكسيكي', NULL, NULL, NULL, NULL),
(154, 'ماليزيا', 'Malaysia', 'ماليزي', NULL, NULL, NULL, NULL),
(155, 'موزمبيق', 'Mozambique', 'موزمبيقي', NULL, NULL, NULL, NULL),
(156, 'ناميبيا', 'Namibia', 'ناميبي', NULL, NULL, NULL, NULL),
(157, 'كاليدونيا الجديدة', 'New Caledonia', 'كاليدوني', NULL, NULL, NULL, NULL),
(158, 'النيجر', 'Niger', 'نيجري', NULL, NULL, NULL, NULL),
(159, 'جزيرة نورفولك', 'Norfolk Island', 'نورفولكي', NULL, NULL, NULL, NULL),
(160, 'نيكاراغوا', 'Nicaragua', 'نيكاراغوي', NULL, NULL, NULL, NULL),
(161, 'هولندا', 'Netherlands', 'هولندي', NULL, NULL, NULL, NULL),
(162, 'النرويج', 'Norway', 'نرويجي', NULL, NULL, NULL, NULL),
(163, 'نيبال', 'Nepal', 'نيبالي', NULL, NULL, NULL, NULL),
(164, 'نيوي', 'Niue', 'نيوي', NULL, NULL, NULL, NULL),
(165, 'نيوزيلندا', 'New Zealand', 'نيوزيلندي', NULL, NULL, NULL, NULL),
(166, 'بناما', 'Panama', 'بنامي', NULL, NULL, NULL, NULL),
(167, 'بيرو', 'Peru', 'بيروي', NULL, NULL, NULL, NULL),
(168, 'بولينيزيا الفرنسية', 'French Polynesia', 'بولينيزي', NULL, NULL, NULL, NULL),
(169, 'بابوا غينيا الجديدة', 'French Guiana', 'بابوا غيني', NULL, NULL, NULL, NULL),
(170, 'الفلبين', 'Philippines', 'فلبيني', NULL, NULL, NULL, NULL),
(171, 'باكستان', 'Pakista', 'باكستاني', NULL, NULL, NULL, NULL),
(172, 'بولندا', 'Poland', 'بولندي', NULL, NULL, NULL, NULL),
(173, 'سان بيار وميكلون', 'Saint Pierre and Miquelo', 'سان بيار وميكلون', NULL, NULL, NULL, NULL),
(174, 'بتكايرن', 'Pitcairn Islands', 'بتكايرني', NULL, NULL, NULL, NULL),
(175, 'بورتوريكو', 'Puerto Rico', 'بورتوريكي', NULL, NULL, NULL, NULL),
(176, 'البرتغال', 'Portugal', 'برتغالي', NULL, NULL, NULL, NULL),
(177, 'بالاو', 'Palau', 'بالاوي', NULL, NULL, NULL, NULL),
(178, 'ريونيون', 'Réunio', 'ريونيوني', NULL, NULL, NULL, NULL),
(179, 'رومانيا', 'Romania', 'روماني', NULL, NULL, NULL, NULL),
(180, 'صربيا', 'Serbia', 'صربي', NULL, NULL, NULL, NULL),
(181, 'الاتحاد الروسي', 'Russia', 'روسي', NULL, NULL, NULL, NULL),
(182, 'رواندا', 'Rwanda', 'رواندي', NULL, NULL, NULL, NULL),
(183, 'جزر سليمان', 'Solomon Islands', 'جزر سليمان', NULL, NULL, NULL, NULL),
(184, 'سيشيل', 'Seychelles', 'سيشيلي', NULL, NULL, NULL, NULL),
(185, 'السويد', 'Swede', 'سويدي', NULL, NULL, NULL, NULL),
(186, 'سنغافورة', 'Singapore', 'سنغافوري', NULL, NULL, NULL, NULL),
(187, 'سانت هيلانة وأسنسيون وتريستان دا كونها', 'Saint Helena', 'سانت هيلانة وأسنسيون وتريستان دا كونها', NULL, NULL, NULL, NULL),
(188, 'سلوفينيا', 'Slovenia', 'سلوفيني', NULL, NULL, NULL, NULL),
(189, 'سلوفاكيا (جمهورية سلوفاكيا),', 'Slovakia', 'سلوفاكي', NULL, NULL, NULL, NULL),
(190, 'سيراليون', 'Sierra Leone', 'سيراليوني', NULL, NULL, NULL, NULL),
(191, 'سان مارينو', 'San Marino', 'سان ماريني', NULL, NULL, NULL, NULL),
(192, 'السنغال', 'Senegal', 'سنغالي', NULL, NULL, NULL, NULL),
(193, 'سورينام', 'Suriname', 'سورينامي', NULL, NULL, NULL, NULL),
(194, 'ساو تومي وبرينسيبي', 'São Tomé and Príncipe', 'ساو تومي وبرينسيبي', NULL, NULL, NULL, NULL),
(195, 'السلفادور', 'El Salvador', 'سلفادوري', NULL, NULL, NULL, NULL),
(196, 'سوازيلاند', 'Switzerland', 'سوازيلاندي', NULL, NULL, NULL, NULL),
(197, 'تشاد', 'Chad', 'تشادي', NULL, NULL, NULL, NULL),
(198, 'توغو', 'To', 'توغولي', NULL, NULL, NULL, NULL),
(199, 'تايلاند', 'Thailand', 'تايلاندي', NULL, NULL, NULL, NULL),
(200, 'طاجيكستان', 'Tajikista', 'طاجيكستاني', NULL, NULL, NULL, NULL),
(201, 'تيمور الشرقية', 'Timor-Leste', 'تيموري', NULL, NULL, NULL, NULL),
(202, 'تركمانستان', 'Turkmenista', 'تركمانستاني', NULL, NULL, NULL, NULL),
(203, 'تونغا', 'Tonga', 'تونغي', NULL, NULL, NULL, NULL),
(204, 'تركيا', 'Turkey', 'تركي', NULL, NULL, NULL, NULL),
(205, 'ترينيداد وتوباغو', 'Trinidad and Toba', 'ترينيدادي', NULL, NULL, NULL, NULL),
(206, 'توفالو', 'Tuvalu', 'توفالي', NULL, NULL, NULL, NULL),
(207, 'تايوان', 'Taiwa', 'تايواني', NULL, NULL, NULL, NULL),
(208, 'تنزانيا', 'Tanzania', 'تنزاني', NULL, NULL, NULL, NULL),
(209, 'الولايات المتحدة', 'United States', 'أمريكي', NULL, NULL, NULL, NULL),
(210, 'أوروغواي', 'Uruguay', 'أوروغواياني', NULL, NULL, NULL, NULL),
(211, 'أوزبكستان', 'Uzbekista', 'أوزبكستاني', NULL, NULL, NULL, NULL),
(212, 'الفاتيكان', NULL, 'فاتيكاني', NULL, NULL, NULL, NULL),
(213, 'سانت فنسنت وجزر غرينادين', 'Saint Vincent and the Grenadines', 'سانت فنسنت وجزر غرينادين', NULL, NULL, NULL, NULL),
(214, 'فنزويلا', 'Venezuela', 'فنزويلي', NULL, NULL, NULL, NULL),
(215, 'جزر فيرجن البريطانية', 'UK Virgin Islands', 'جزر فيرجن البريطانية', NULL, NULL, NULL, NULL),
(216, 'جزر فرجن الأمريكية', 'US Virgin Islands', 'جزر فرجن الأمريكية', NULL, NULL, NULL, NULL),
(217, 'فيتنام', 'Vietnam', 'فيتنامي', NULL, NULL, NULL, NULL),
(218, 'فانواتو', 'Vanuatu', 'فانواتي', NULL, NULL, NULL, NULL),
(219, 'ساموا', 'Samoa', 'سامي', NULL, NULL, NULL, NULL),
(220, 'جنوب أفريقيا', 'South Africa', 'جنوبي أفريقي', NULL, NULL, NULL, NULL),
(221, 'زامبيا', 'Zambia', 'زامبي', NULL, NULL, NULL, NULL),
(222, 'زيمبابوي', 'Zimbabwe', 'زيمبابوياني', NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`CountryId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `CountryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=223;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
