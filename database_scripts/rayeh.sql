-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2018 at 10:08 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rayeh`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `is_super_admin` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `username`, `password`, `mobile`, `email`, `is_super_admin`, `created_at`, `updated_at`) VALUES
(1, 'مدير النظام', 'admin', '123123', NULL, NULL, 1, '2018-10-30 02:14:09', '0000-00-00 00:00:00'),
(2, 'أحمد1', 'أحمد', '123', '(059) 654-1155', 'admin@admin.com', 0, '2018-10-20 19:17:53', '2018-10-20 16:17:53'),
(3, 'تجربة', 'test', '123', '(059) 654-1155', NULL, 0, '2018-10-20 16:20:15', '2018-10-20 16:20:15');

-- --------------------------------------------------------

--
-- Table structure for table `advertisments`
--

CREATE TABLE `advertisments` (
  `id` int(11) NOT NULL,
  `image` longblob NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `url` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `carriers`
--

CREATE TABLE `carriers` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status_id` int(11) NOT NULL,
  `transfer_way_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `from_place` varchar(50) NOT NULL,
  `to_place` varchar(50) NOT NULL,
  `transfer_type` tinyint(1) NOT NULL,
  `people_count` int(5) DEFAULT NULL,
  `from_point_x` float NOT NULL,
  `to_point_x` float NOT NULL,
  `from_point_y` float NOT NULL,
  `to_point_y` float NOT NULL,
  `notes` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `carriers`
--

INSERT INTO `carriers` (`id`, `order_id`, `date`, `status_id`, `transfer_way_id`, `from_date`, `to_date`, `from_place`, `to_place`, `transfer_type`, `people_count`, `from_point_x`, `to_point_x`, `from_point_y`, `to_point_y`, `notes`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-10-23', 1, 2, '2018-10-26', '2018-10-31', 'جدة', 'مكة', 1, 5, 0, 0, 0, 0, '', 1, '2018-10-24 01:40:54', '0000-00-00 00:00:00'),
(2, 2, '2018-10-16', 3, 2, '2018-10-31', '2018-11-30', 'دبي', 'لوس انجلوس', 2, 0, 0, 0, 0, 0, '', 2, '2018-10-24 01:40:54', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `carriers_orders`
--

CREATE TABLE `carriers_orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `status_id` tinyint(1) NOT NULL,
  `carrier_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `carriers_orders`
--

INSERT INTO `carriers_orders` (`id`, `user_id`, `from_date`, `to_date`, `status_id`, `carrier_id`, `created_at`, `updated_at`) VALUES
(1, 2, '2018-10-26', '2018-10-31', 1, 1, '2018-10-26 07:40:55', '0000-00-00 00:00:00'),
(2, 3, '2018-11-01', '2018-10-27', 2, 1, '2018-10-26 07:40:55', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `constant_table`
--

CREATE TABLE `constant_table` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `constant_table`
--

INSERT INTO `constant_table` (`id`, `name`, `type`) VALUES
(1, 'مفعل', 1),
(2, 'موقوف', 1),
(3, 'تم حذفه', 1);

-- --------------------------------------------------------

--
-- Table structure for table `customers_tours`
--

CREATE TABLE `customers_tours` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status_id` int(11) NOT NULL,
  `transfer_way_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `from_place` varchar(50) NOT NULL,
  `to_place` varchar(50) NOT NULL,
  `transfer_type` tinyint(1) NOT NULL,
  `people_count` int(5) NOT NULL,
  `from_point_x` float NOT NULL,
  `to_point_x` float NOT NULL,
  `from_point_y` float NOT NULL,
  `to_point_y` float NOT NULL,
  `notes` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customers_tours_orders`
--

CREATE TABLE `customers_tours_orders` (
  `id` int(11) NOT NULL,
  `customer_tour_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `status_id` tinyint(1) NOT NULL COMMENT '1- مقبول / 2- مرفوض',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `notify_text` text NOT NULL,
  `admin_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status_id` tinyint(1) NOT NULL COMMENT '1- ارسل 2- الغي',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `status_id` int(11) NOT NULL,
  `delete_reason` text,
  `work_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `mobile`, `password`, `email`, `city`, `country`, `status_id`, `delete_reason`, `work_id`, `created_at`, `updated_at`) VALUES
(1, 'محمد على', '05995522661', '05995522661', 'info@dd.ps', 'pp', 'pp', 1, '', 0, '2018-10-22 17:20:18', '0000-00-00 00:00:00'),
(2, 'test', '05995522661', '05995522661', 'ff', 'ff', 'ff', 2, '', 0, '2018-10-22 17:20:18', '0000-00-00 00:00:00'),
(3, 'محمد على', '05995522661', '05995522661', 'info@dd.ps', 'pp', 'pp', 1, '', 1, '2018-10-22 17:21:03', '0000-00-00 00:00:00'),
(4, 'test', '05995522661', '05995522661', 'ff', 'ff', 'ff', 2, '', 1, '2018-10-22 17:21:03', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advertisments`
--
ALTER TABLE `advertisments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carriers`
--
ALTER TABLE `carriers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carriers$user_id_fk` (`user_id`),
  ADD KEY `carriers$status_id_fk` (`status_id`),
  ADD KEY `carriers$transfer_way_id_fk` (`transfer_way_id`);

--
-- Indexes for table `carriers_orders`
--
ALTER TABLE `carriers_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `carrier_id` (`carrier_id`);

--
-- Indexes for table `constant_table`
--
ALTER TABLE `constant_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers_tours`
--
ALTER TABLE `customers_tours`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customersTour$status_id_fk` (`status_id`),
  ADD KEY `customersTour$transfer_way_id_fk` (`transfer_way_id`),
  ADD KEY `customersTour$user_id_fk` (`user_id`);

--
-- Indexes for table `customers_tours_orders`
--
ALTER TABLE `customers_tours_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customersTourOrders$customer_tour_id_fk` (`customer_tour_id`),
  ADD KEY `customersTourOrders$user_id_fk` (`user_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications$admin_id_fk` (`admin_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_id_fk` (`status_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `advertisments`
--
ALTER TABLE `advertisments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `carriers`
--
ALTER TABLE `carriers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `carriers_orders`
--
ALTER TABLE `carriers_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `constant_table`
--
ALTER TABLE `constant_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customers_tours`
--
ALTER TABLE `customers_tours`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers_tours_orders`
--
ALTER TABLE `customers_tours_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `carriers`
--
ALTER TABLE `carriers`
  ADD CONSTRAINT `carriers$status_id_fk` FOREIGN KEY (`status_id`) REFERENCES `constant_table` (`id`),
  ADD CONSTRAINT `carriers$transfer_way_id_fk` FOREIGN KEY (`transfer_way_id`) REFERENCES `constant_table` (`id`),
  ADD CONSTRAINT `carriers$user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `carriers_orders`
--
ALTER TABLE `carriers_orders`
  ADD CONSTRAINT `carriers_orders$carrier_id_fk` FOREIGN KEY (`carrier_id`) REFERENCES `carriers` (`id`),
  ADD CONSTRAINT `carriers_orders$user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `customers_tours`
--
ALTER TABLE `customers_tours`
  ADD CONSTRAINT `customersTour$status_id_fk` FOREIGN KEY (`status_id`) REFERENCES `constant_table` (`id`),
  ADD CONSTRAINT `customersTour$transfer_way_id_fk` FOREIGN KEY (`transfer_way_id`) REFERENCES `constant_table` (`id`),
  ADD CONSTRAINT `customersTour$user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `customers_tours_orders`
--
ALTER TABLE `customers_tours_orders`
  ADD CONSTRAINT `customersTourOrders$customer_tour_id_fk` FOREIGN KEY (`customer_tour_id`) REFERENCES `customers_tours` (`id`),
  ADD CONSTRAINT `customersTourOrders$user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications$admin_id_fk` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `status_id_fk` FOREIGN KEY (`status_id`) REFERENCES `constant_table` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
