ALTER TABLE `users` CHANGE `password` `password` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `carriers` ADD `cancel_reason` TEXT NULL AFTER `distance`;
ALTER TABLE `carriers_orders` CHANGE `status_id` `status_id` TINYINT(1) NULL;