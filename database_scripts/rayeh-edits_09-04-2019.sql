ALTER TABLE `carrier_trips_stations` DROP `waypoint`;
ALTER TABLE `carriers` ADD `track` TEXT NOT NULL AFTER `price`;
ALTER TABLE `carriers` CHANGE `track` `track` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `carriers` CHANGE `from_place` `from_place` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
CHANGE `to_place` `to_place` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `carriers_orders` ADD `alone` TINYINT(1) NULL AFTER `notes`, ADD `i_have_things` TINYINT(1) NULL AFTER `alone`, 
ADD `seats_count` INT(11) NULL AFTER `i_have_things`, ADD `weight` TINYINT(1) NULL AFTER `seats_count`, 
ADD `size` TINYINT(1) NULL AFTER `weight`;