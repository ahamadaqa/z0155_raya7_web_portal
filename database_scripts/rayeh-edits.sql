ALTER TABLE `users` CHANGE `email` `email` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `carriers_orders` ADD `price` INT(11) NOT NULL AFTER `carrier_id`, ADD `notes` TEXT NOT NULL AFTER `price`;
ALTER TABLE `carriers_orders` CHANGE `notes` `notes` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `customers_tours_orders` ADD `price` INT(11) NOT NULL AFTER `status_id`, ADD `notes` TEXT NOT NULL AFTER `price`;
ALTER TABLE `customers_tours_orders` CHANGE `notes` `notes` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `customers_tours_orders` CHANGE `from_date` `from_date` DATE NULL, CHANGE `to_date` `to_date` DATE NULL;
ALTER TABLE `carriers_orders` CHANGE `from_date` `from_date` DATE NULL, CHANGE `to_date` `to_date` DATE NULL;
