ALTER TABLE `carriers` CHANGE `people_count` `person_count` INT(5) NULL DEFAULT NULL;
ALTER TABLE `carriers` CHANGE `order_id` `order_id` INT(9) ZEROFILL NOT NULL;
ALTER TABLE `customers_tours` CHANGE `people_count` `person_count` INT(5) NOT NULL;
ALTER TABLE `customers_tours` CHANGE `order_id` `order_id` INT(9) ZEROFILL NOT NULL;
ALTER TABLE `customers_tours` ADD `track` TEXT NOT NULL AFTER `accept_transfer_others`;


