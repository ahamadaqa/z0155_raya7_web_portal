ALTER TABLE `cusromer_tour_stations` DROP `longitude`;
ALTER TABLE `cusromer_tour_stations` CHANGE `latitude` `waypoint` TEXT NOT NULL;
ALTER TABLE `carriers` ADD `flexible_minutes_before` INT(11) NOT NULL AFTER `updated_at`, 
ADD `flexible_minutes_after` INT(11) NOT NULL AFTER `flexible_minutes_before`, 
ADD `flexible_days_before` INT(11) NOT NULL AFTER `flexible_minutes_after`, 
ADD `flexible_days_after` INT(11) NOT NULL AFTER `flexible_days_before`, 
ADD `men_count` INT(11) NOT NULL AFTER `flexible_days_after`, 
ADD `women_count` INT(11) NOT NULL AFTER `men_count`, 
ADD `children_count` INT(11) NOT NULL AFTER `women_count`, 
ADD `babies_count` INT(11) NOT NULL AFTER `children_count`, 
ADD `things_count` INT(11) NOT NULL AFTER `babies_count`, 
ADD `weight` TINYINT(1) NOT NULL AFTER `things_count`, 
ADD `size` TINYINT(1) NOT NULL AFTER `weight`, 
ADD `want_repeat` TINYINT(1) NOT NULL AFTER `size`, 
ADD `dont_want_smoke` TINYINT(1) NOT NULL AFTER `want_repeat`, 
ADD `dont_want_music` TINYINT(1) NOT NULL AFTER `dont_want_smoke`, 
ADD `car_brand_id` INT(11) NOT NULL AFTER `dont_want_music`;

ALTER TABLE `customers_tours` ADD `price` FLOAT NOT NULL AFTER `car_brand_id`;
ALTER TABLE `carriers` ADD `price` FLOAT NOT NULL AFTER `car_brand_id`;

DROP TABLE `cusromer_tour_stations`;

CREATE TABLE `carrier_trips_stations` (
  `id` int(11) NOT NULL,
  `carrier_trip_id` int(11) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `waypoint` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `carrier_trips_stations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carrier_trip_id_fk` (`carrier_trip_id`);
ALTER TABLE `carrier_trips_stations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `carrier_trips_stations`
  ADD CONSTRAINT `carrier_trip_id_fk` FOREIGN KEY (`carrier_trip_id`) REFERENCES `carriers` (`id`);

CREATE TABLE `carrier_trip_repeats` (
  `id` int(11) NOT NULL,
  `carrier_trip_id` int(11) NOT NULL,
  `every_day` tinyint(1) NOT NULL,
  `every_sat` tinyint(1) NOT NULL,
  `every_sun` tinyint(1) NOT NULL,
  `every_mon` tinyint(1) NOT NULL,
  `every_tue` tinyint(1) NOT NULL,
  `every_wed` int(11) NOT NULL,
  `every_thr` tinyint(1) NOT NULL,
  `every_fri` tinyint(1) NOT NULL,
  `end_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `carrier_trip_repeats`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `carrier_trip_repeats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `carriers` CHANGE `transfer_type` `transfer_type` TINYINT(1) NULL;
ALTER TABLE `carriers` CHANGE `distance` `distance` FLOAT NULL;
ALTER TABLE `carriers` CHANGE `flexible_minutes_before` `flexible_minutes_before` INT(11) NOT NULL DEFAULT '0', CHANGE `flexible_minutes_after` `flexible_minutes_after` INT(11) NOT NULL DEFAULT '0', CHANGE `flexible_days_before` `flexible_days_before` INT(11) NOT NULL DEFAULT '0', CHANGE `flexible_days_after` `flexible_days_after` INT(11) NOT NULL DEFAULT '0', CHANGE `men_count` `men_count` INT(11) NOT NULL DEFAULT '0', CHANGE `women_count` `women_count` INT(11) NOT NULL DEFAULT '0', CHANGE `children_count` `children_count` INT(11) NOT NULL DEFAULT '0', CHANGE `babies_count` `babies_count` INT(11) NOT NULL DEFAULT '0', CHANGE `things_count` `things_count` INT(11) NOT NULL DEFAULT '0', CHANGE `want_repeat` `want_repeat` TINYINT(1) NOT NULL DEFAULT '0', CHANGE `dont_want_smoke` `dont_want_smoke` TINYINT(1) NOT NULL DEFAULT '0', CHANGE `dont_want_music` `dont_want_music` TINYINT(1) NOT NULL DEFAULT '0';

ALTER TABLE `customers_tours` ADD `accept_transfer_others` TINYINT(1) NOT NULL DEFAULT '0' AFTER `price`;