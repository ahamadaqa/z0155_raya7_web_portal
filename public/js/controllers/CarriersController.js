/* Setup blank page controller */
angular.module('MetronicApp').controller('CarriersController', ['$rootScope', '$scope', 'Carriers', function($rootScope, $scope, Carriers) {
    $rootScope.resource = Carriers;
        $rootScope.init();
        
        $scope._reset = function () {
            $scope.row = {};
            $rootScope.init();
        };
        
        $scope._search = function (row) {
            $rootScope.resource.query(row, function (data) {
                $rootScope.details = data.data;
                $rootScope.CurrentPage = data.current_page;
                $rootScope.TotalItems = data.total;
                $rootScope.ItemsPerPage = data.per_page;
            
        });
        };
}]);

MetronicApp.controller('CarriersCtrlAdd', ['$scope', 'Carriers', '$stateParams', '$rootScope', function ($scope, Carriers, $stateParams, $rootScope) {

        if ($stateParams.id) {
            Carriers.get({'id': $stateParams.id}, function (data) {
                $scope.row = data.carriers;
                $scope.orders = data.orders;
                $scope.evaluations = data.evaluations;
            });
        }

              

    }]);