/* Setup blank page controller */
angular.module('MetronicApp').controller('CustomersController', ['$rootScope', '$scope', 'Customers', function($rootScope, $scope, Customers) {
    $rootScope.resource = Customers;
        $rootScope.init();
        
        $scope._reset = function () {
            $scope.row = {};
            $rootScope.init();
        };
        
        $scope._search = function (row) {
            $rootScope.resource.query(row, function (data) {
                $rootScope.details = data.data;
                $rootScope.CurrentPage = data.current_page;
                $rootScope.TotalItems = data.total;
                $rootScope.ItemsPerPage = data.per_page;
            
        });
        };
}]);

MetronicApp.controller('CustomersCtrlAdd', ['$scope', 'Customers', '$stateParams', '$rootScope', function ($scope, Customers, $stateParams, $rootScope) {

        if ($stateParams.id) {
            Customers.get({'id': $stateParams.id}, function (data) {
                $scope.row = data.carriers;
                $scope.orders = data.orders;
                $scope.evaluations = data.evaluations;
            });
        }

              

    }]);