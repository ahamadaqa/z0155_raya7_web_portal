/* Setup blank page controller */
angular.module('MetronicApp').filter('split', function () {
    return function (input, splitChar, splitIndex) {
        // do some bounds checking here to ensure it has that index
        return input.split(splitChar)[splitIndex];
    }
}).controller('UsersController', ['$rootScope', '$scope', 'Users', function($rootScope, $scope, Users) {
    $rootScope.resource = Users;
        $rootScope.init();
        
        $scope._reset = function () {
            $scope.row = {};
            $rootScope.init();
        };
        
        $scope._search = function (row) {
            $rootScope.resource.query(row, function (data) {
                $rootScope.details = data.data;
                $rootScope.CurrentPage = data.current_page;
                $rootScope.TotalItems = data.total;
                $rootScope.ItemsPerPage = data.per_page;
            
        });
        };
}]);

MetronicApp.controller('UsersCtrlAdd', ['$scope', 'Users', '$stateParams', '$rootScope', function ($scope, Users, $stateParams, $rootScope) {


        $rootScope.RemoveMsg();

        if ($stateParams.id) {
            $scope.title = 'تعديل';
            Users.get({'id': $stateParams.id}, function (data) {
                $scope._edit(data.data);
                $scope.status = data.status;
                $scope.carriersPoints = data.carriersPoints;
                $scope.carriersAvg = data.carriersAvg;
                $scope.customersPoints = data.customersPoints;
                $scope.customersAvg = data.customersAvg;
                $scope.total_distace = 0;
                for(var i=0; i<data.data.carriers.length; i++){
                    $scope.total_distace += parseFloat(data.data.carriers[i].distance);
                }
                
                for(var i=0; i<data.data.customers.length; i++){
                    $scope.total_distace += parseFloat(data.data.customers[i].distance);
                }
                
                $scope.total_distace = $scope.total_distace.toFixed(2); 
                
            });
        }
        else {
            $scope.title = 'إضافة';
            $scope.mode = "add";
        }


        $rootScope.clear_msg();
        $scope.row = {};
        $scope.enableReson = true;
        $scope.activeReson = function(id){
           if(id==3){
           $scope.enableReson = false;    
           } else {
             $scope.enableReson = true;   
           }
        }

        $scope._edit = function (item) {

            $scope.mode = 'edit';
            $scope.row = item;
            $scope.row.status_id = item.status_id+"";

        };

        $scope._save = function (row) {
            if ($scope.mode == 'edit') {
                Users.modify({'data': row, id: $scope.row.id}, function (data) {
                    $rootScope.check_save_result(data, "/users");
                });

            } else {
                Users.create(row, function (data) {
                    $rootScope.check_save_result(data, "/users");
                });

            }
        };

        $scope._cancel = function () {
            $rootScope.cancel("/users");
        };

    }]);