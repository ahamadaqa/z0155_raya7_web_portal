var MyAngularServices = angular.module('MyAngularServices', []).constant('API_URL', 'api/v1.0/');

MyAngularServices.factory('Admins', ['$resource', function ($resource) {
        return $resource('Admins/:operation/:id', {id: '@id'}, {
            query: {method: 'GET'},
            create: {method: 'POST'},
            modify: {method: 'PUT'},
            destroy: {method: 'DELETE'}
        });
    }]);

MyAngularServices.factory('Users', ['$resource', function ($resource) {
        return $resource('Users/:operation/:id', {id: '@id'}, {
            query: {method: 'GET'},
            create: {method: 'POST'},
            modify: {method: 'PUT'},
            destroy: {method: 'DELETE'}
        });
    }]);

MyAngularServices.factory('Carriers', ['$resource', function ($resource) {
        return $resource('Carriers/:operation/:id', {id: '@id'}, {
            query: {method: 'GET'},
            create: {method: 'POST'},
            modify: {method: 'PUT'},
            destroy: {method: 'DELETE'}
        });
    }]);

MyAngularServices.factory('Customers', ['$resource', function ($resource) {
        return $resource('Customers/:operation/:id', {id: '@id'}, {
            query: {method: 'GET'},
            create: {method: 'POST'},
            modify: {method: 'PUT'},
            destroy: {method: 'DELETE'}
        });
    }]);

MyAngularServices.factory('Advertisments', ['$resource', function ($resource) {
        return $resource('Advertisments/:operation/:id', {id: '@id'}, {
            query: {method: 'GET'},
            create: {method: 'POST'},
            modify: {method: 'PUT'},
            destroy: {method: 'DELETE'}
        });
    }]);

MyAngularServices.factory('Notifications', ['$resource', function ($resource) {
        return $resource('Notifications/:operation/:id', {id: '@id'}, {
            query: {method: 'GET'},
            create: {method: 'POST'},
            modify: {method: 'PUT'},
            destroy: {method: 'DELETE'}
        });
    }]);

MyAngularServices.factory('Authenticate', ['$resource', 'API_URL', function ($resource, API_URL) {
        return $resource(':operation/:id', {id: '@id'}, {
            logout: {method: 'POST', params: {operation: 'logout'}},
            user: {method: 'GET', params: {operation: 'devenv/user'}},
        });
    }]);


